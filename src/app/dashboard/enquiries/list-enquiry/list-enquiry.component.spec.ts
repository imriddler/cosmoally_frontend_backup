import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEnquiryComponent } from './list-enquiry.component';

describe('ListEnquiryComponent', () => {
  let component: ListEnquiryComponent;
  let fixture: ComponentFixture<ListEnquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEnquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
