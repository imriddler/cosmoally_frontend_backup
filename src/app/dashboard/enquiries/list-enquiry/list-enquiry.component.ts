import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {EnquiryListResponse} from '../../models/app.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-list-enquiry',
  templateUrl: './list-enquiry.component.html',
  styleUrls: ['./list-enquiry.component.scss']
})
export class ListEnquiryComponent implements OnInit {

  enquiries = [];
  apiLoaded = false;
  p = 1;

  constructor(
    private toastr: ToastrService,
    private restService: RestService
  ) {

  }

  ngOnInit() {
    this.getEnquiryList();
  }

  getEnquiryList() {
    this.restService.getEnquiries().subscribe(res => {
      const response = res as EnquiryListResponse;
      if (response.status) {
        this.enquiries = response.data.enquiries;
        this.apiLoaded = true;
      } else {
        this.apiLoaded = true;
        this.toastr.error('Sorry could not fetch enquiries', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

}
