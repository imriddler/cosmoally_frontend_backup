import { Component, OnInit } from '@angular/core';
import {EnquiryViewResponse, Page} from '../../models/app.model';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {BaseResponse} from '../../../base-response';

@Component({
  selector: 'app-view-enquiry',
  templateUrl: './view-enquiry.component.html',
  styleUrls: ['./view-enquiry.component.scss']
})
export class ViewEnquiryComponent implements OnInit {

  id: number;
  enquiry = {
    id : null as number,
    title : '',
    comment : '',
    name: '',
    email: '',
    user : null
  };
  p = null as number;

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.id = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.getEnquiryDetail();
  }

  getEnquiryDetail() {
    this.restService.getEnquiryDetail(this.id).subscribe(res => {
      const response = res as EnquiryViewResponse;
      if (response.status) {
        this.enquiry = response.data.enquiry;
      } else {
        this.toastr.error('Sorry could not get enquiry detail', 'Error');
      }
    } , error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

}
