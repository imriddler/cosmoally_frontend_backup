import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { RestService } from '../../rest.service';
import { BannerListResponse, ImagesResultModel } from '../models/app.model';
import { BaseResponse } from '../../base-response';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {

  banners = [];
  apiLoaded = false;
  fileToUpload: File[] = [];

  editBanner = {
    id: null as number,
    title: '',
    description: '',
    imageUrl: '',
    fullImageUrl: ''
  };

  editBannerResult = {
    id: null as number,
    fullUrl: '',
    imageUrl: ''
  };

  bannerResult = {
    id: null as number,
    fullUrl: '',
    imageUrl: ''
  };

  modalRef: BsModalRef;

  banner = {
    id: null as number,
    title: '',
    description: '',
    imageUrl: ''
  };

  imageShown = true;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location,
    private modal: BsModalService
  ) {
  }

  extentionsAllowed = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG'];
  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  ngOnInit() {
    this.getBannerList();
  }

  getBannerList() {
    this.restService.getBannerList().subscribe(res => {
      const response = res as BannerListResponse;
      this.apiLoaded = true;
      if (response.status) {
        this.banners = response.data.banners;
      } else {
        this.toastr.error('Sorry could not fetch banners.');
      }
    }, error => {
      this.toastr.error(error.error.message);
    });
  }

  readUrl(event) {
    this.fileToUpload = [];
    const image = event.target.files.item(0);
    if (!this.extentionsAllowed.includes(image.name.split('.')[image.name.split('.').length - 1])) {
      this.toastr.warning('Only JPG and PNG files are supported.');
      this.fileToUpload = [];
      return;
    }
    this.fileToUpload.push(image);
    this.uploadImage();
  }

  validateRequest() {
    if (this.banner.imageUrl.length === 0) {
      this.toastr.warning('Please select banner image');
      return false;
    }else  if (this.banner.title.length > 50) {
      this.toastr.warning('Banner title can not be longer than 50 characters.');
      return false;
    } else if (this.banner.description.length > 100 ) {
      this.toastr.warning('Banner description should not be longer than 100 characters.');
      return false;
    } else {
      return true;
    }
  }

  addBanner() {
    this.banner.imageUrl = this.bannerResult.imageUrl;
    if (this.validateRequest()) {
      this.restService.addBanner(this.banner).subscribe(res => {
        this.toastr.success('Banners added successfully');
        this.hideModal();
        this.getBannerList();
      }, error => {
        this.toastr.error(error.error.massage);
      });
    }
  }

  openAddBannerModal(template: TemplateRef<any>) {
    this.modalRef = this.modal.show(template, { class: 'modal-lg' });
  }

  uploadImage() {
    this.restService.uploadImages(this.fileToUpload)
      .subscribe(data => {
        const response = data as ImagesResultModel;
        this.bannerResult = {
          id: null as number,
          fullUrl: '',
          imageUrl: ''
        };
        response.data.images.forEach(image => {
          this.bannerResult = {
            id: image.id,
            fullUrl: image.fullUrl,
            imageUrl: image.url
          };
        });
      },
        error => {
          this.toastr.error(error.error.message, 'Error');
        });
  }

  deleteBanner(id: any) {
    this.restService.deleteBanner(id).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Banner deleted successfully.');
        this.getBannerList();
      } else {
        this.toastr.error('Sorry could not delete banner.');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }

  hideModal() {
    this.modalRef.hide();

    this.banner = {
      id: null as number,
      title: '',
      description: '',
      imageUrl: '',
    };

    this.editBanner = {
      id: null as number,
      title: '',
      description: '',
      imageUrl: '',
      fullImageUrl: ''
    };

    this.bannerResult = {
      id: null as number,
      imageUrl: '',
      fullUrl: ''
    };

    this.editBannerResult = {
      id: null as number,
      imageUrl: '',
      fullUrl: ''
    };
  }

  readEditUrl(event) {
    this.fileToUpload = [];
    const image = event.target.files.item(0);
    if (!this.extentionsAllowed.includes(image.name.split('.')[image.name.split('.').length - 1])) {
      this.toastr.warning('Only JPG and PNG files are supported.');
      this.fileToUpload = [];
      return;
    }
    this.fileToUpload.push(image);
    this.uploadEditImage();
  }

  uploadEditImage() {
    this.restService.uploadImages(this.fileToUpload)
      .subscribe(data => {
          const response = data as ImagesResultModel;
          this.editBannerResult = {
            id: null as number,
            fullUrl: '',
            imageUrl: ''
          };
          response.data.images.forEach(image => {
            this.editBannerResult = {
              id: image.id,
              fullUrl: image.fullUrl,
              imageUrl: image.url
            };
          });
          this.imageShown = true;
        },
        error => {
          this.toastr.error(error.error.message, 'Error');
        });
  }

  openEditBannerModal(template: TemplateRef<any>, id) {
    this.modalRef = this.modal.show(template, { class: 'modal-lg' });
    this.editBanner = this.banners.find(item => {
      return item.id === id;
    });
    this.editBannerResult.fullUrl = this.editBanner.fullImageUrl;
    this.editBannerResult.imageUrl = this.editBanner.imageUrl;
  }

  validateEditRequest() {
    if (this.editBanner.imageUrl.length === 0) {
      this.toastr.warning('Please select banner image');
      return false;
    }else  if (this.editBanner.title.length > 50) {
      this.toastr.warning('Banner title can not be longer than 50 characters.');
      return false;
    } else if (this.editBanner.description.length > 100 ) {
      this.toastr.warning('Banner description should not be longer than 100 characters.');
      return false;
    } else {
      return true;
    }
  }

  updateBanner() {
    this.editBanner.imageUrl = this.editBannerResult.imageUrl;
    if (this.validateEditRequest()) {
      this.restService.editBanner(this.editBanner, this.editBanner.id).subscribe(res => {
        const response =  res as BaseResponse;
        if (response.status) {
          this.toastr.success('Banners updated successfully');
          this.hideModal();
          this.getBannerList();
          this.editBanner = {
            id: null as number,
            title: '',
            description: '',
            imageUrl: '',
            fullImageUrl: ''
          };
        } else {
          this.toastr.error('Sorry could not update banner', 'Error');
        }
      }, error => {
        this.toastr.error(error.error.massage, 'Oops');
      });
    }
  }

  removeImage() {
    this.editBannerResult = {
      id: null as number,
      fullUrl: '',
      imageUrl: ''
    };
    this.imageShown = false;
  }
}
