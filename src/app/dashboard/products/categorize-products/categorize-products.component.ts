import {Component, OnInit, TemplateRef} from '@angular/core';
import {RestService} from '../../../rest.service';
import {
  Product,
  ProductCategoryResponse,
  ProductDetailResponse,
  ProductListResponse,
  ProductStatusListResponse,
  CategorizationProductListResponse, ProductCategory
} from '../../models/app.model';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {BaseResponse} from '../../../base-response';
import {Location} from '@angular/common';

@Component({
  selector: 'app-categorize-products',
  templateUrl: './categorize-products.component.html',
  styleUrls: ['./categorize-products.component.scss']
})
export class CategorizeProductsComponent implements OnInit {

  multipleDropDownSettings = {};
  categoryList = [];
  productStatusList = [];
  batchNumber = '';
  isBatchApproved = false;
  p = null as number;

  apiLoaded = false;

  model = {
    batchNumber: '',
    userName: '',
    uploadedAt: '',
    isSaved: false,
    hasError: false,
    isApproved: false,
    updateCount: 0,
    products: [] as {
      id: number;
      name: string;
      imageUrl: string;
      productCategoryId: number;
      statusCode: number;
      statusText: string;
      message: string;
    }[]
  };

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private modal: BsModalService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.batchNumber = route.snapshot.paramMap.get('batchId');
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }

  ngOnInit() {
    this.multipleDropDownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 4,
      allowSearchFilter: false,
      enableCheckAll: true,
      maxHeight: 100
    };


    // Getting Category List
    this.restService.getProductCategoryList().subscribe(res => {
      const response = res as ProductCategoryResponse;
      if (response.status) {
        this.categoryList = response.data.categories;
      } else {
        this.toastr.error('Sorry could not get categories.');
      }
    });

    this.restService.getProductStatusList().subscribe(res => {
      const response = res as ProductStatusListResponse;
      if (response.status) {
        this.productStatusList = response.data.statusList;
      } else {
        this.toastr.error('Sorry could not get status list', 'Error');
      }
    }, error => {
      this.toastr.error(error.message, 'Error');
    });

    // Getting Categorized Product List
    this.refreshProductList();

  }

  // Refresh Product List
  refreshProductList() {
    this.restService.getProductList(this.batchNumber).subscribe(res => {
      const response = res as CategorizationProductListResponse;
      if (response.status) {
        this.model.batchNumber = response.data.batchNumber;
        this.model.userName = response.data.userName;
        this.model.isApproved = response.data.isApproved;
        this.model.isSaved = response.data.isSaved;
        this.model.updateCount = response.data.updateCount;
        this.model.hasError = response.data.hasError;
        this.model.uploadedAt = response.data.uploadedAt;
        response.data.products.forEach(element => {
          this.model.products.push({
            id: element.id,
            name: element.name,
            imageUrl: element.imageUrl,
            productCategoryId: element.category == null ? element.category : element.category.id,
            statusCode: element.statusCode,
            statusText: element.statusText,
            message: element.message
          });
        });
        this.isBatchApproved = this.isBatchApprovable();
      } else {
        this.toastr.error('Sorry could not fetch product list', 'Error');
      }
    }, error => {
      this.toastr.error(error.message, 'Error');
    });
  }

  isBatchApprovable() {
    let output = true;
    this.model.products.forEach(item => {
      if (item.statusCode > 1) {
        output = false;
      } else {
        if (item.productCategoryId === null) {
          output = false;
        }
      }
    });
    return output;
  }

  handleCategoryChange(product) {
    this.isBatchApproved = this.isBatchApprovable();
  }

  handleStatusChange(product) {
    this.isBatchApproved = this.isBatchApprovable()
    if (product.statusCode > 1) {
      product.productCategoryId = null as number;
      product.message = this.productStatusList[product.statusCode - 1].message;
    } else {
      product.message = '';
    }
  }

  submitApprove() {
      this.model.isApproved = true;
      if(this.validateSaveRequest()){
        this.restService.updateProduct(this.model, this.model.batchNumber).subscribe(res => {
          const response = res as BaseResponse;
          if (response.status) {
            this.toastr.success('Batch Approved successfully');
            this.location.back();
          } else {
            this.toastr.error('Sorry could not approve batch', 'Error');
          }
        }, error => {
          this.toastr.error(error.error.message, 'Error');
        });
      }
  }

  validateSaveRequest() {
    let output = true;
    this.model.products.forEach((item,index) => {
      if(item.statusCode === null){
        this.toastr.warning('Please select status on ' + item.name);
        output = false;
      } else if (item.statusCode === 1) {
        if (item.productCategoryId === null) {
          this.toastr.warning('Please select category on ' + item.name);
          output = false;
        }
      } else if (item.statusCode > 1) {
        if (item.message.length === 0 ) {
          this.toastr.warning('Please enter message text on ' + item.name);
          output = false;
        }
      }
    });

    return output;
  }


  getCategoryName(id: number) {
    const category = this.categoryList.find( item => {
      return item.id === id;
    });
    if(category == null){
      return '';
    } else {
      return category.name;
    }
  }

  submitSave() {
    this.model.isApproved = false;
    if (this.validateSaveRequest()) {
      this.restService.updateProduct(this.model, this.model.batchNumber).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('Product saved successfully');
          this.location.back();
        } else {
          this.toastr.error('Sorry could not save Product', 'Error');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Error');
      });
    }
  }

}
