import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BatchPreviewResponse, BatchPreview, Product } from '../../models/app.model';
import { ToastrService } from 'ngx-toastr';
import {RestService} from '../../../rest.service';

@Component({
  selector: 'app-batch-preview',
  templateUrl: './batch-preview.component.html',
  styleUrls: ['./batch-preview.component.scss']
})
export class BatchPreviewComponent implements OnInit {

  batch = {
    batchNumber: '',
    categories : [],
    isApproved : false,
    isSaved: false,
    updateCount: null as number,
    uploadedAt: '',
    userName: ''
  }

  batchNumber = '';

  constructor(
    private location: Location,
    private restService: RestService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.batchNumber = this.route.snapshot.paramMap.get('batch');
  }

  ngOnInit() {
    this.restService.getBatchPreview(this.batchNumber).subscribe(res => {
      const response = res as BatchPreviewResponse;
      if (response.status) {
        this.batch = response.data.batch;
      } else {
        this.toastr.error('Sorry, could not fetch preview for batch : ' + this.batchNumber, 'Oops');
      }
    }, error => {
      this.toastr.error(error.message, 'Error');
    });
  }

  goBack(){
    this.location.back();
  }

}
