import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchPreviewComponent } from './batch-preview.component';

describe('BatchPreviewComponent', () => {
  let component: BatchPreviewComponent;
  let fixture: ComponentFixture<BatchPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
