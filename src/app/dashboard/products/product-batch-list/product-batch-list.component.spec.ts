import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBatchListComponent } from './product-batch-list.component';

describe('ProductBatchListComponent', () => {
  let component: ProductBatchListComponent;
  let fixture: ComponentFixture<ProductBatchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBatchListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
