import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {RestService} from '../../../rest.service';
import {ProductBatch, ProductBatchListResponse} from '../../models/app.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-batch-list',
  templateUrl: './product-batch-list.component.html',
  styleUrls: ['./product-batch-list.component.scss']
})
export class ProductBatchListComponent implements OnInit {

  productBatches: ProductBatch[] = [];
  filteredProductBatches: ProductBatch[] = [];
  apiLoaded = false;
  p = 1;

  // Filter Variables
  savedFilter = false;
  approvedFilter = false;
  newFilter = false;
  allFilter = true;
  isAllFilterDisabled = true;

  constructor(
    private toastr: ToastrService,
    private restService: RestService,
    private route: ActivatedRoute
    ) { 
      this.route.queryParams.subscribe(value =>  {
        this.p = value.page ? value.page : 1;
      });
    }

  ngOnInit() {
    this.restService.getProductBatchList().subscribe(res => {
      this.apiLoaded = true;
      const response = res as ProductBatchListResponse;
      if (response.status) {
        this.productBatches = response.data.batches;
        this.filteredProductBatches = response.data.batches;
      } else {
        this.toastr.error('Sorry could not fetch products', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }
  

  showAll(){
    this.allFilter = true;
    this.newFilter = false;
    this.savedFilter = false;
    this.approvedFilter = false;
    this.isAllFilterDisabled = true;
    this.filterProducts()
  }


  manageChecks(){
    if(this.newFilter && this.savedFilter && this.approvedFilter){
      this.allFilter = true;
      this.isAllFilterDisabled = true;
    } else if(this.newFilter || this.savedFilter || this.approvedFilter) {
      this.isAllFilterDisabled = false;
      this.allFilter = false;
    }
  }


  // Method to Filter Products
  filterProducts() {
    this.manageChecks();
    this.filteredProductBatches = this.productBatches.filter(item => {
      if (!this.newFilter && !this.savedFilter && !this.approvedFilter) {
        this.allFilter = true;
        this.isAllFilterDisabled = true;
        return true;
      } 
      if (this.newFilter) {
        if (!item.isApproved && !item.isSaved) {
          return true;
        }
      }
      if (this.savedFilter) {
        if ( !item.isApproved && item.isSaved) {
          return true;
        }
      }
      if (this.approvedFilter) {
        if (item.isSaved &&  item.isApproved) {
          return true;
        }
      }
    });
  }

}
