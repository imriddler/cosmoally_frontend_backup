import {Component, OnInit} from '@angular/core';
import {ProductCategory, ProductCategoryResponse, ViewProductCategoryResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {BaseResponse} from '../../../base-response';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit-product-category',
  templateUrl: './edit-product-category.component.html',
  styleUrls: ['./edit-product-category.component.scss']
})
export class EditProductCategoryComponent implements OnInit {


  apiLoaded = false;
  id = null as number;

  model = {
    id: null as number,
    name: '',
    description: '',
    categoryCost: null as number,
    perVariantCost: null as number,
  };

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location
  ) {
    this.id = this.route.snapshot.paramMap.get('id') as any as number;
  }


  ngOnInit() {
    this.viewProductCategoryDetail();
  }

  viewProductCategoryDetail() {
    this.restService.viewProductCategoryDetail(this.id).subscribe(res => {
      const response = res as ViewProductCategoryResponse;
      if (response.status) {
        this.model = response.data.productCategory;
        this.apiLoaded = true;
      } else {
        this.toastr.error('Could not fetch category detail.');
        this.apiLoaded = true;
      }
    });
  }

  validateRequest() {
    const floatRegex = /^[+-]?\d+(\.\d+)?$/;
    if (this.model.name.trim().length === 0) {
      this.toastr.warning('Please enter category name.');
      return false;
    } else if (this.model.name.trim().length > 185) {
      this.toastr.warning('Category name length should less than 185 characters.');
      return false;
    } else if (!isNaN(Number(this.model.name))) {
      this.toastr.warning('Category name can not be a number');
      return false;
    } else
    // } else if (this.model.description.trim().length === 0) {
    //   this.toastr.warning('Please enter description.');
    //   return false;
    //
    if (this.model.description.trim().length > 150) {
      this.toastr.warning('Category description length should less than 150 characters.');
      return false;
    } else if (String(this.model.categoryCost).trim().length === 0) {
      this.toastr.warning('Please enter category cost.');
      return false;
    } else if (!floatRegex.test(String(this.model.categoryCost))) {
      this.toastr.warning('Category cost should be in number.');
      return false;
    } else if (String(this.model.categoryCost).split('.')[0].length > 6) {
      this.toastr.warning('Category cost should be less than 100 Million.');
      return false;
    } else if (String(this.model.categoryCost).includes('-')) {
      this.toastr.warning('Category cost should be positive.');
      return false;
    } else if (String(this.model.categoryCost).includes('.')) {
      this.toastr.warning('Decimal not allowed in category cost.');
      return false;
    } else if (this.model.perVariantCost == null) {
      this.toastr.warning('Please enter the per variant cost.');
      return false;
    } else if (!floatRegex.test(String(this.model.perVariantCost))) {
      this.toastr.warning('Per variant cost should be in numbers.');
      return false;
    }  else if (String(this.model.perVariantCost).includes('-')) {
      this.toastr.warning('Per variant cost should be positive.');
      return false;
    } else if (String(this.model.perVariantCost).split('.')[0].length > 6) {
      this.toastr.warning('Per variant cost should be less than 100 Million.');
      return false;
    } else if (String(this.model.perVariantCost).includes('.')) {
      this.toastr.warning('Decimal not allowed in per variant cost.');
      return false;
    } else {
      return true;
    }
  }


  editCategory() {
    if (this.validateRequest()) {
      this.restService.editProductCategory(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('Product category updated successfully.');
          this.router.navigateByUrl('/product/categories');
        } else {
          this.toastr.error('Sorry could not update category.');
        }
      },error => {
          this.toastr.error(error.error.message, 'Error');
        });
    }
  }


  goBack() {
    this.location.back();
  }

}
