import { Component, OnInit } from '@angular/core';
import {RestService} from '../../../rest.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ViewProductCategoryResponse} from '../../models/app.model';

@Component({
  selector: 'app-view-product-category',
  templateUrl: './view-product-category.component.html',
  styleUrls: ['./view-product-category.component.scss']
})
export class ViewProductCategoryComponent implements OnInit {

  category = {
    id : null as number,
    name: '',
    description: '',
    perVariantCost: null as number,
    categoryCost: null as number
  };

  id = null as number;

  constructor(
    private restService: RestService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.id = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.restService.getProductCategoryDetail(this.id).subscribe(res => {
      const response = res as ViewProductCategoryResponse;
      if (response.status) {
        this.category = response.data.productCategory;
        console.log(this.category);
      } else {
        this.toastr.error(response.message, 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }

}
