import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductLogResponse} from '../../models/app.model';

@Component({
  selector: 'app-batch-log',
  templateUrl: './batch-log.component.html',
  styleUrls: ['./batch-log.component.scss']
})
export class BatchLogComponent implements OnInit {

  batchNumber = '';
  logs = [];
  apiLoaded = false;
  p = 1;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.batchNumber = route.snapshot.paramMap.get('batch');
  }

  ngOnInit() {
    this.restService.getBatchLog(this.batchNumber).subscribe(res => {
      this.apiLoaded = true;
      const response = res as ProductLogResponse;
      if (response.status) {
        this.logs = response.data.productlogs;
      } else {
        this.toastr.error('Sorry could not fetch logs.', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  getLocalTime(the_date:Date){
    let date = new Date(the_date);
    let offset = date.getTimezoneOffset();
    let date2 = new Date(date.getTime() + ((-offset) * 60000))
    return date2
  }


}
