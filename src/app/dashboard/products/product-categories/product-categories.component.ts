import {Component, OnInit} from '@angular/core';
import {ProductCategory, ProductCategoryResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {BaseResponse} from '../../../base-response';

@Component({
  selector: 'app-product-categories',
  templateUrl: './product-categories.component.html',
  styleUrls: ['./product-categories.component.scss']
})
export class ProductCategoriesComponent implements OnInit {

  categories: ProductCategory[] = [];
  apiLoaded = false;
  p = 1;

  model = {
    id: null,
    name: '',
    description: '',
    categoryCost: null as Number,
    perVariantCost: null as Number,
  };

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location
  ) {
  }


  ngOnInit() {
    this.getCategoryList();
  }

  getCategoryList() {
    this.restService.getProductCategoryList().subscribe(res => {
      const response = res as ProductCategoryResponse;
      if (response.status) {
        this.categories = response.data.categories as ProductCategory[];
        this.apiLoaded = true;
      } else {
        this.toastr.error('Could not fetch categories.');
        this.apiLoaded = true;
      }
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }

  validateRequest() {
    const floatRegex = /^[+-]?\d+(\.\d+)?$/;
    if (this.model.name.trim().length === 0) {
      this.toastr.warning('Please enter category name.');
      return false;
    } else if (this.model.name.trim().length >= 185) {
      this.toastr.warning('Category name length should less than 185 characters.');
      return false;
    } else if (!isNaN(Number(this.model.name))) {
      this.toastr.warning('Category name can not be a number');
      return false;
    } else
    /*
    * } else if (this.model.description.trim().length === 0) {
    *   this.toastr.warning('Please enter description.');
    *  return false;
    *}
    */
    if (this.model.description.trim().length > 150) {
      this.toastr.warning('Category description length should less than 150 characters.');
      return false;
    } else if (this.model.categoryCost === null) {
      this.toastr.warning('Please enter category cost.');
      return false;
    } else if (!floatRegex.test(String(this.model.categoryCost))) {
      this.toastr.warning('Category cost should be in number.');
      return false;
    } else if (String(this.model.categoryCost).split('.')[0].length > 6) {
      this.toastr.warning('Category cost should be less than 100 Million.');
      return false;
    } else if (String(this.model.categoryCost).includes('-')) {
      this.toastr.warning('Category cost should be positive.');
      return false;
    } else if (String(this.model.categoryCost).includes('.')) {
      this.toastr.warning('Decimal is not allowed in category cost.');
      return false;
    } else if (this.model.perVariantCost === null) {
      this.toastr.warning('Please enter per variant cost.');
      return false;
    } else if (!floatRegex.test(String(this.model.perVariantCost))) {
      this.toastr.warning('Per variant cost should be in numbers.');
      return false;
    } else if (String(this.model.perVariantCost).split('.')[0].length > 6) {
      this.toastr.warning('Per variant cost should be less than 100 Million.');
      return false;
    } else if (String(this.model.perVariantCost).includes('-')) {
      this.toastr.warning('Per variant cost should be positive.');
      return false;
    } else if (String(this.model.perVariantCost).includes('.')) {
      this.toastr.warning('Decimal is not allowed in per variant cost.');
    } else {
      return true;
    }
  }


  addCategory() {
    if (this.validateRequest()) {
      this.restService.addProductCategory(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.getCategoryList();
          this.toastr.success('Product category added successfully.');
          this.model = {
            id: null,
            name: '',
            description: '',
            categoryCost: null as Number,
            perVariantCost: null as Number,
          };
        } else {
          this.toastr.error('Sorry could not add category.');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Error');
      });
    }
  }

  deleteCategory(id) {
      this.restService.deleteProductCategory(id).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('Category deleted successfully.');
          this.getCategoryList();
        } else {
          this.toastr.error('Sorry could not delete category.');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Error');
      });
  }

  goBack() {
    this.location.back();
  }

}
