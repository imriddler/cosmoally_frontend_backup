import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardRoutingModule} from './dashboard.routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import {UserModule} from './user/user.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TokenInterceptor} from '../auth/token.interceptor';
import {AuthorizationInterceptor} from '../auth/authorization.interceptor';
import { KnowledgeBoxComponent } from './knowledge-box/knowledge-box.component';
import { AddFaqComponent } from './knowledge-box/add-faq/add-faq.component';
import { EditFaqComponent } from './knowledge-box/edit-faq/edit-faq.component';
import { ViewFaqComponent } from './knowledge-box/view-faq/view-faq.component';
import { FormsModule } from '@angular/forms';
import { AddPageComponent } from './page/add-page/add-page.component';
import { EditPageComponent } from './page/edit-page/edit-page.component';
import { ListPageComponent } from './page/list-page/list-page.component';
import { ViewPageComponent } from './page/view-page/view-page.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CategoryComponent } from './knowledge-box/category/category.component';
import { ProductCategoriesComponent } from './products/product-categories/product-categories.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { EditProductCategoryComponent } from './products/edit-product-category/edit-product-category.component';
import { CategorizeProductsComponent } from './products/categorize-products/categorize-products.component';
import { NotificationComponent } from './notification/notification.component';
import {UserAddComponent} from './user/user-add/user-add.component';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserDetailComponent} from './user/user-detail/user-detail.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {ChangePasswordComponent} from './user/change-password/change-password.component';
import {BsDatepickerModule, DatepickerModule, TimepickerModule} from 'ngx-bootstrap';
import { ProductListComponent } from './user-products/product-list/product-list.component';
import { AppUserListComponent } from './user-products/app-user-list/app-user-list.component';
import { SendNotificationComponent } from './notification/send-notification/send-notification.component';
import {BannersComponent} from './banners/banners.component';
import { ViewNotificationComponent } from './notification/view-notification/view-notification.component';
import { ViewProductCategoryComponent } from './products/view-product-category/view-product-category.component';
import { ProductBatchListComponent } from './products/product-batch-list/product-batch-list.component';
import { BatchPreviewComponent } from './products/batch-preview/batch-preview.component';
import { ServicesComponent } from './services/services.component';
import { NotificationUsersComponent } from './notification/notification-users/notification-users.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { NotificationEditComponent } from './notification/notification-edit/notification-edit.component';
import {MarkdownModule} from 'ngx-markdown';
import { BatchLogComponent } from './products/batch-log/batch-log.component';
import { ReadFaqListComponent } from './user-products/read-faq-list/read-faq-list.component';
import { ListEnquiryComponent } from './enquiries/list-enquiry/list-enquiry.component';
import { ViewEnquiryComponent } from './enquiries/view-enquiry/view-enquiry.component';
import { AppUpdatesComponent } from './app-updates/app-updates.component';
import { AddUpdateComponent } from './app-updates/add-update/add-update.component';


@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
    KnowledgeBoxComponent,
    AddFaqComponent,
    EditFaqComponent,
    ViewFaqComponent,
    AddPageComponent,
    EditPageComponent,
    ListPageComponent,
    ViewPageComponent,
    CategoryComponent,
    ProductCategoriesComponent,
    EditProductCategoryComponent,
    CategorizeProductsComponent,
    NotificationComponent,
    UserAddComponent,
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
    ChangePasswordComponent,
    ProductListComponent,
    AppUserListComponent,
    BannersComponent,
    SendNotificationComponent,
    ViewNotificationComponent,
    ViewProductCategoryComponent,
    ProductBatchListComponent,
    BatchPreviewComponent,
    ServicesComponent,
    NotificationUsersComponent,
    NotificationEditComponent,
    BatchLogComponent,
    ReadFaqListComponent,
    ListEnquiryComponent,
    ViewEnquiryComponent,
    AppUpdatesComponent,
    AddUpdateComponent
  ],
  imports: [
    CommonModule,
    UserModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DashboardRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    MarkdownModule.forChild(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'warning' // set defaults here
    }),
    DatepickerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ],
})
export class DashboardModule {
}
