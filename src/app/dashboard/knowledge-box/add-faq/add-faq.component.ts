import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {RestService} from '../../../rest.service';
import {BaseResponse} from '../../../base-response';
import {Category, CategoryResponse, Faq, ImageModel, ImagesResultModel, UploadedImage} from '../../models/app.model';
import {ActivatedRoute, Router} from '@angular/router';

// @ts-ignore
@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})
export class AddFaqComponent implements OnInit {

  categoryList: Category[] = [];
  // uploadedImages: UploadedImage[] = [];
  // selected_image: any;
  editorConfig = {};
  model = {
    id: 0 as null,
    question: '',
    answer: '',
    // sortOrder: null as number,
    faqTypes: [],
    // images: []
  };

  p = null as number;

  multipleDropDownSettings = {};

  constructor(
    private toastr: ToastrService,
    public restService: RestService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }

  ngOnInit() {

    this.restService.getFaqTypeList().subscribe(res => {
      const response = res as CategoryResponse;
      if (response.status) {
        this.categoryList = response.data.faqTypes as Category[];
        // console.log(this.categoryList);
      } else {
        this.toastr.error('Sorry could not fetch category.');
      }
    }, error => {
      this.toastr.error(error);
    });

    this.multipleDropDownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 3,
      allowSearchFilter: false,
      enableCheckAll: true,
      maxHeight: 100
    };

    this.editorConfig = {
      'editable': true,
      'spellcheck': true,
      'height': '300px',
      'minHeight': '100px',
      'translate': 'no',
      'uploadUrl': this.restService.apiUrl + 'image/upload'
    };
  }

  // readURL(event) {
  //   if (event.target.files) {
  //     const filesToUpload: File[] = [];
  //     for (let i = 0; i < event.target.files.length; i++) {
  //       filesToUpload.push(event.target.files[i]);
  //     }
  //     this.restService.uploadImages(filesToUpload).subscribe(res => {
  //       const response = res as ImagesResultModel;
  //       if (response.status) {
  //         response.data.images.forEach((element: UploadedImage) => {
  //           this.uploadedImages.push(element);
  //           this.model.images.push({
  //             id: element.id,
  //             imageType: 1,
  //             imageUrl: element.url
  //           });
  //         });
  //         this.selected_image = '';
  //       }
  //     });
  //   }
  // }
  //
  // deleteImage(id: Number) {
  //   this.restService.deleteImage(id).subscribe(res => {
  //     const response = res as ImagesResultModel;
  //     if (response.status) {
  //       let url = '';
  //       const index = this.uploadedImages.findIndex((element: UploadedImage) => {
  //         if (element.id === id) {
  //           url = element.url;
  //           return true;
  //         }
  //       });
  //       this.uploadedImages.splice(index, 1);
  //       this.model.images = this.model.images.filter((element: ImageModel) => {
  //         if (element.imageUrl != url) {
  //           return true;
  //         }
  //       });
  //     } else {
  //       this.toastr.error('Sorry could not delete image.');
  //     }
  //   });
  // }

  validateRequest() {
    if (this.model.question.trim().length === 0) {
      this.toastr.warning('Question is required.');
      return false;
    } else if (this.model.question.length > 65000) {
      this.toastr.warning('Question should be less than 65000 characters.');
      return false;
    } else if (this.model.answer.trim().length === 0) {
      this.toastr.warning('Answer is required.');
      return false;
    } else if (this.model.faqTypes.length === 0) {
      this.toastr.warning('At least one faq type is required.');
      return false;
    } else {
      return true;
    }
    // else if (this.model.sortOrder == null) {
    //   this.toastr.warning('Please enter sort order');
    //   return false;
    // }
    // else if (this.model.sortOrder === 0) {
    //   this.toastr.warning('Sort order can not be zero.');
    //   return false;
    // }
  }

  submit() {
    if (this.validateRequest()) {
      this.restService.addFaq(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.router.navigate(['/faqs'], {queryParams: {page: this.p}});
        } else {
          this.toastr.error('Sorry could not add faq.');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

}
