import {Component, OnInit} from '@angular/core';
import {RestService} from '../../rest.service';
import {FaqListResponse, Faq} from '../models/app.model';
import {ToastrService} from 'ngx-toastr';
import {BaseResponse} from '../../base-response';

@Component({
  selector: 'app-knowledge-box',
  templateUrl: './knowledge-box.component.html',
  styleUrls: ['./knowledge-box.component.scss']
})
export class KnowledgeBoxComponent implements OnInit {

  faqList = [];
  p = 1;
  apiLoaded = false;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getFaqs();
  }

  getFaqs() {
    this.restService.getFaqList().subscribe(res => {
      const response = res as FaqListResponse;
      if (response.status) {
        this.faqList = [];
        response.data.faqs.forEach(item => {
          if (item.faqTypes.length > 0) {
            let faq = item;
            let faqType = []
            item.faqTypes.forEach(faqtype => {
              faqType.push(faqtype.name);
            })
            faq.faqTypes = faqType;
            this.faqList.push(faq);
          }
        });
        this.apiLoaded = true;
      } else {
        this.toastr.error('Sorry could not fetch the faqs.');
        this.apiLoaded = true;
      }
    });
  }

  delete(id: number) {
      this.restService.deleteFaq(id).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('FAQ deleted Successfully');
          this.getFaqs();
        } else {
          this.toastr.error('Sorry could not delete FAQ.');
        }
      });
  }



}
