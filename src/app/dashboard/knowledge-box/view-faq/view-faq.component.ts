import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../rest.service';
import { ActivatedRoute } from '@angular/router';
import {Category, Faq, FaqDetailResponse, ImageModel} from '../../models/app.model';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-faq',
  templateUrl: './view-faq.component.html',
  styleUrls: ['./view-faq.component.scss']
})
export class ViewFaqComponent implements OnInit {

  faq = {
    id: null as number,
    question: '',
    answer: '',
    // sortOrder: null as number,
    faqTypes: [],
    // images: []
  };
  id: number;
  p  = null as number;

  whichView = 1;
  userId = null as number;

  constructor(
    private restService: RestService,
    private route: ActivatedRoute,
    private location: Location,
    private toastr: ToastrService
  ) {
    this.id = this.route.snapshot.paramMap.get('id') as any as number;
    route.queryParams.subscribe( value => {
      this.p = value.page;
      this.whichView = value.view;
      if(this.whichView > 1){
        this.userId = value.userId;
      }
    });
  }

  ngOnInit() {
    this.restService.getFaq(this.id).subscribe(res => {
      const response = res as FaqDetailResponse;
      if (response.status) {
        this.faq = response.data.faq as Faq;
      } else {
        this.toastr.error(response.message);
      }
    });
  }

}
