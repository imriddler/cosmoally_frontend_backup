import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Category, CategoryResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {BaseResponse} from '../../../base-response';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categories: Category[] = [];
  apiLoaded = false;

  model = {
    id: null as number,
    name: '',
    sortOrder: null as number
  };

  p = null as number;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  isEditMode = false;

  constructor(
    public restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }


  ngOnInit() {
    this.getCategoryList();
  }

  getCategoryList() {
    this.restService.getFaqTypeList().subscribe(res => {
      const response = res as CategoryResponse;
      if (response.status) {
        this.categories = response.data.faqTypes as Category[];
        this.apiLoaded = true;
      }
    }, err => {
      this.apiLoaded = true;
    });
  }

  addCategory() {
    if (this.validateRequest()) {
      this.restService.addCategory(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('FAQ category added successfully');
          this.getCategoryList();
          this.model = {
            id: null as number,
            name: '',
            sortOrder: null as number
          };
        } else {
          this.toastr.warning('Sorry Could not add Category');
        }
      }, errRes => {
        this.toastr.warning(errRes.error.message);
      });
    }
  }

  deleteCategory(id) {
      this.restService.deleteCategory(id).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('FAQ category deleted successfully');
          this.getCategoryList();
        } else {
          this.toastr.warning('Sorry Could Not Delete Category');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
  }

  validateRequest() {
    if (this.model.name.trim().length === 0) {
      this.toastr.warning('Category name is required.');
      return false;
    } else if (this.model.name.length > 150) {
      this.toastr.warning('Category name length should be less than 150 characters.');
      return false;
    } else if (this.model.sortOrder == null ) {
      this.toastr.warning('Please enter sort order');
      return false;
    } else if (this.model.sortOrder == 0) {
      this.toastr.warning('Sort order can not be zero');
      return false;
    } else {
      return true;
    }
  }



  editCategory() {
    if (this.validateRequest()) {
      this.restService.editFaqCategory(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.model = {
            id: null as number,
            name: '',
            sortOrder: null as number
          };
            this.toastr.success('FAQ category updated successfully.');
          this.isEditMode = false;
          this.getCategoryList();
        } else {
          this.toastr.error('Sorry could not update category.');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

  toEditMode(id) {
    const index = this.categories.findIndex(item => {
      return item.id === id;
    });
    this.model.id = this.categories[index].id;
    this.model.name = this.categories[index].name;
    this.model.sortOrder = this.categories[index].sortOrder;
    this.isEditMode = true;
  }

}
