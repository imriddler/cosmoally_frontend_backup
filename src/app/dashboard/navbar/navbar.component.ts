import {Component, OnInit, Renderer} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../auth/auth.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  userId = '';
  userName = '';
  constructor(private _toastr: ToastrService,
              private renderer: Renderer,
              private _authService: AuthService,
              private _router: Router) {
  }

  ngOnInit() {
    this._router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.userName = this._authService.getUserName();
      }
    });
    this.userId = this._authService.getUserId();
    this.userName = this._authService.getUserName();
  }


  logout() {
    this._authService.removeToken();
    this._router.navigate(['/login']);
  }
}
