import {BaseResponse} from '../../base-response';


// Image Models

class ImageModel {
  id;
  imageUrl;
  imageType;
}

class UploadedImage {
  id: number;
  url: string;
  fullUrl: string;
}

class ImagesResultModel extends BaseResponse {
  data: {
    images: UploadedImage[];
  };
}


// Page Models

class Page {
  id: number;
  title: string;
  content: string;
  sortOrder: null | number;
  pageType: number;
  images: ImageModel[];
  data: [{
    id: '',
    question: '',
    answer: ''
  }];
}

class PageResponse extends BaseResponse {
  data: {
    pages: Page[];
  };
}

class PageDetailResponse extends BaseResponse {
  data: {
    page: Page;
  };
}


// Banner Models

class Banner {
  id: null;
  imageUrl: string;
  imageType: 2;
}

class BannerListResponse extends BaseResponse {
  data: {
    banners: Banner[];
  };
}

class Faq {
  id: number;
  question: string;
  answer: string;
  // sortOrder: number;
  faqTypes: Category[];
  // images: ImageModel[];
}

class Category {
  id: number;
  name: string;
  sortOrder: number;
}

class CategoryResponse extends BaseResponse {
  data: {
    faqTypes: Category[];
  };
}

class CategoryViewResponse extends BaseResponse {
  data: {
    faqType: Category;
  };
}

class FaqListResponse extends BaseResponse {
  data: {
    faqs: Faq[];
  };
}

class FaqDetailResponse extends BaseResponse {
  data: {
    faq: Faq;
  };
}

class UserFaqReadListResponse extends BaseResponse {
  data: {
    user: UserModel;
    faqs: Faq[];
  }
}


// Product Models

class ProductCategory {
  id = null;
  name;
  description;
  categoryCost;
  perVariantCost;
}

class ProductCategoryResponse extends BaseResponse {
  data: {
    categories: ProductCategory[];
  };
}

class ViewProductCategoryResponse extends BaseResponse {
  data: {
    productCategory: ProductCategory;
  };
}

class ProductStatusListResponse extends BaseResponse {
  data: {
    statusList: {
      id: string;
      name: string;
    }[];
  };
}

class Product {
  id: number;
  name: string;
  imageSize: string;
  imageUrl: string;
  category: null | ProductCategory;
  statusCode: number;
  sequence: string;
  statusText: string;
  message: string;
}

class ProductDetailResponse extends BaseResponse {
  data: {
    product: Product;
  };
}

class ProductListResponse extends BaseResponse {
  data: {
    products: Product[];
  };
}

class CategorizationProductListResponse extends BaseResponse {
  data: {
    batchNumber: string;
    userName: string;
    uploadedAt: string;
    isSaved: boolean;
    hasError: boolean;
    isApproved: false;
    updateCount: number;
    products: Product[];
  };
}

class ProductBatch {
  userName: string;
  batchNumber: string;
  productCount: number;
  products: Product[];
  uploadedAt: string;
  isSaved: boolean;
  isApproved: boolean;
  hasError: boolean;
  updateCount: number;
}

class ProductLog {
  activity: any;
  batch: any;
  createdAt: any;
  event: any;
  newValues: any;
  oldValues: any;
  product: any;
  user: any;
}

class ProductLogResponse  extends BaseResponse {
  data: {
    productlogs: ProductLog[];
  };
}


class BatchPreview {
  batchNumber: string;
  uploadedAt: string;
  updateCount: number;
  userName: string;
  isSaved: boolean;
  isApproved: boolean;
  categories: {
    category: ProductCategory,
    products: Product[];
  }[];
}

class ProductBatchListResponse extends BaseResponse {
  data: {
    batches: ProductBatch[];
  };
}

class BatchPreviewResponse extends BaseResponse {
  data: {
    batch: BatchPreview;
  };
}

class LoginResponse extends BaseResponse {
  data: {
    user: any;
    token: string;
  };
}


// User Models

class UserAddModel {
  name: string;
  email: string;
  password: string;
}

class UserListResponse extends BaseResponse {
  data: {
    users: UserModel[];
  };
}

class UserViewResponse extends BaseResponse {
  data: {
    users: UserModel;
  };
}

class UserDetailResponse extends BaseResponse {
  data: {
    user: UserModel;
  };
}

class UserModel {
  id: number;
  name: string;
  email: string;
  latitude: null | number;
  longitude: null | number;
  loginType: null | number;
  isActive: boolean;
  isAdmin: boolean;
  isEmailVerified: boolean;
  batches: ProductBatch[];
  faqRead: number;
  faqLimit: number;
}


// Notification Models

class Notification {
  id: number;
  title: string;
  message: string;
  isAllSelected: boolean;
  status: string;
  users: {
    id: number;
    name: string;
    userId: number;
  }[];
  scheduledAt: string;
}

class NotificationListResponse extends BaseResponse {
  data: {
    notifications: Notification[];
  };
}

class NotificationDetailResponse extends BaseResponse {
  data: {
    notification: Notification;
  };
}

class NotificationUsersListResponse extends BaseResponse {
  data: {
    users: UserModel[];
  };
}


// Service Models

class Service {
  id: number;
  name: string;
  sortOrder: number;
  listType: number;
}

class ServiceDetailResponse extends BaseResponse {
  data: {
    service: Service;
  };
}

class ServiceListResponse extends BaseResponse {
  data: {
    serviceList: Service[];
  };
}


// Enquiry Models

class Enquiry {
  id: number;
  title: string;
  comment: string;
  name: string;
  email: string;
  user: UserModel;
}

class EnquiryListResponse extends BaseResponse {
  data: {
    enquiries: Enquiry[];
  };
}

class EnquiryViewResponse extends BaseResponse {
  data: {
    enquiry: Enquiry;
  };
}

class AppUpdate{
  platform: string;
  version: string;
  live_at: string;
  update_required: boolean;
  update_text: string;
}

class AppUpdateResponse extends BaseResponse {
  data: {
   updates: AppUpdate[];
  } 
}

export {
  ImageModel,
  Page,
  PageResponse,
  PageDetailResponse,
  Faq,
  Category,
  CategoryResponse,
  FaqListResponse,
  FaqDetailResponse,
  UploadedImage,
  ImagesResultModel,
  ProductCategory,
  ProductCategoryResponse,
  BannerListResponse,
  Product,
  ProductListResponse,
  UserAddModel,
  UserListResponse,
  BatchPreviewResponse,
  BatchPreview,
  UserViewResponse,
  UserDetailResponse,
  UserModel,
  ViewProductCategoryResponse,
  CategorizationProductListResponse,
  Notification,
  NotificationListResponse,
  NotificationDetailResponse,
  NotificationUsersListResponse,
  ProductBatch,
  ProductBatchListResponse,
  ProductLog,
  ProductLogResponse,
  ProductStatusListResponse,
  ProductDetailResponse,
  Service,
  ServiceListResponse,
  ServiceDetailResponse,
  UserFaqReadListResponse,
  Enquiry,
  EnquiryListResponse,
  EnquiryViewResponse,
  AppUpdate,
  AppUpdateResponse
};
