import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {BaseResponse} from '../../../base-response';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-send-notification',
  templateUrl: './send-notification.component.html',
  styleUrls: ['./send-notification.component.scss']
})
export class SendNotificationComponent implements OnInit {

  userList = [];
  multipleDropDownSettings = {};
  model = {
    title: '',
    message: '',
    isAllSelected: true,
    users: [],
    scheduledAt: ''
  };

  minDate = new Date();

  sendNow = true;
  p = null as number;

  date: Date = null;
  time: Date = null;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {

    route.queryParams.subscribe( value => {
      this.p = value.page;
    });

    this.multipleDropDownSettings = {
      singleSelection: false,
      idField: 'userId',
      textField: 'nameWithEmail',
      itemsShowLimit: 4,
      allowSearchFilter: true,
      enableCheckAll: false,
      maxHeight: 100
    };
  }


  ngOnInit() {
    this.restService.getUsersDropdownList().subscribe(res => {
      const response = res as {
        status: boolean;
        message: string;
        type: string;
        httpStatusCode: number;
        data: {
          users: {
            id: number;
            userId: string;
            name: string;
            email: string;
          }[];
        }
      };
      if (response.status) {
        this.userList = response.data.users;
        this.userList.forEach( item => {
          item.nameWithEmail = item.name + ' (' + item.email + ')';
        });
      } else {
        this.toastr.error('Sorry could not fetch user list.');
      }
    });
  }

  validateRequest() {
    if (this.model.title.trim().length === 0) {
      this.toastr.warning('Please enter title.');
      return false;
    } else if (this.model.message.trim().length === 0) {
      this.toastr.warning('Please enter content.');
      return false;
    } else if (!this.sendNow && (this.date == null || this.time == null)) {
      this.toastr.warning('Please specify the time and date to send notification at.');
      return false;
    } else if (this.model.users.length === 0) {
      if (!this.model.isAllSelected) {
        this.toastr.warning('Please select at least one recipient user.');
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  prepareDateTime() {
    this.model.scheduledAt = '';
    const myDate = new Date();
    if (!this.sendNow) {
      if (this.date !== null) {
        // tslint:disable-next-line:max-line-length
        this.model.scheduledAt += this.date.getFullYear() + '-' + ((this.date.getMonth() + 1) + '').padStart(2, '0') + '-' + (this.date.getDate() + '').padStart(2, '0');
      } else {
        // tslint:disable-next-line:max-line-length
        this.model.scheduledAt += myDate.getFullYear() + '-' + ((myDate.getMonth() + 1) + '').padStart(2, '0') + '-' + (myDate.getDate() + '').padStart(2, '0');
      }
      if (this.time !== null) {
        // tslint:disable-next-line:max-line-length
        this.model.scheduledAt += ' ' + (this.time.getHours() + '').padStart(2, '0') + ':' + (this.time.getMinutes() + '').padStart(2, '0') + ':' + (this.time.getSeconds() + '').padStart(2, '0');
      } else {
        // tslint:disable-next-line:max-line-length
        this.model.scheduledAt += ' ' + (myDate.getHours() + '').padStart(2, '0') + ':' + (myDate.getMinutes() + '').padStart(2, '0') + ':' + (myDate.getSeconds() + '').padStart(2, '0');
      }
    } else {
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += myDate.getFullYear() + '-' + ((myDate.getMonth() + 1) + '').padStart(2, '0') + '-' + (myDate.getDate() + '').padStart(2, '0');
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += ' ' + (myDate.getHours() + '').padStart(2, '0') + ':' + (myDate.getMinutes() + '').padStart(2, '0') + ':' + (myDate.getSeconds() + '').padStart(2, '0');
    }
  }

  sendNotification() {
    if (this.validateRequest()) {
      this.prepareDateTime();
      this.restService.sendNotification(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.model = {
            title: '',
            message: '',
            isAllSelected: true,
            users: [],
            scheduledAt: ''
          };
          this.date = null;
          this.time = null;
          this.toastr.success('Notification sent successfully.');
          this.router.navigateByUrl('/notifications');
        } else {
          this.toastr.error('Sorry could not send notification.');
        }
      });
    }
  }
}
