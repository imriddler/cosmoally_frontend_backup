import { Component, OnInit } from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationUsersListResponse} from '../../models/app.model';
import {BaseResponse} from '../../../base-response';


@Component({
  selector: 'app-notification-users',
  templateUrl: './notification-users.component.html',
  styleUrls: ['./notification-users.component.scss']
})
export class NotificationUsersComponent implements OnInit {

  users = [];

  notificationId = null as number;

  p =  null as number;

  apiLoaded = false;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';
  public popoverBlockMessage = 'Are you really sure you want to block this user?';


  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.notificationId = route.snapshot.paramMap.get('id') as any as number;
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }

  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this.restService.getNotificationUsers(this.notificationId).subscribe(res => {
      const response = res as NotificationUsersListResponse;
      if (response.status) {
        this.users = response.data.users;
      } else {
        this.toastr.error('Sorry could not fetch the users list', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  delete(id){
    this.restService.deleteAppUser(id).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('App user deleted successfully');
        this.router.navigate(['/notifications'],{ queryParams: { page: this.p } } );
      } else {
        this.toastr.error('Sorry could not delete app user', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    })
  }

  block(id){
    this.restService.blockUser(id).subscribe( res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('User blocked successfully');
        this.getUserList();
      } else {
        this.toastr.error('Sorry could not block user', 'Error');
      }
    } , error => {
      this.toastr.error(error.error.message, 'Oops');
    })
  }


  unblock(id){
    this.restService.unBlockUser(id).subscribe( res => {
      const response = res as BaseResponse;
      if (response) {
        this.toastr.success('User unblocked successfully');
        this.getUserList();
      } else {
        this.toastr.error('Sorry could not unblock user', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    })
  }

}
