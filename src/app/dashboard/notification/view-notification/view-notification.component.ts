import { Component, OnInit } from '@angular/core';
import {Notification, NotificationDetailResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-notification',
  templateUrl: './view-notification.component.html',
  styleUrls: ['./view-notification.component.scss']
})
export class ViewNotificationComponent implements OnInit {

  notification = new Notification();
  id = null as number;
  p = null as number;

  constructor(
    private restService: RestService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
    this.id = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.restService.getNotificationDetails(this.id).subscribe(res => {
      const response = res as NotificationDetailResponse;
      if (response.status) {
        this.notification = response.data.notification;
      } else {
        this.toastr.error('Sorry could not get notification details.', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }

}
