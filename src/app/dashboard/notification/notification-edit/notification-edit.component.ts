import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';
import {NotificationDetailResponse, UserListResponse} from '../../models/app.model';
import {BaseResponse} from '../../../base-response';
import {Location} from '@angular/common';

@Component({
  selector: 'app-notification-edit',
  templateUrl: './notification-edit.component.html',
  styleUrls: ['./notification-edit.component.scss']
})
export class NotificationEditComponent implements OnInit {

  notificationId = null as number;

  userList = [];

  model = {
    id: null as number,
    title: '',
    message: '',
    isAllSelected: true,
    users: [],
    scheduledAt: ''
  };

  minDate = new Date();

  p = null as number;

  sendNow = false;

  date: Date = null;
  time: Date = null;


  multipleDropDownSettings = {};

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private location: Location
  ) {

    route.queryParams.subscribe( value => {
      this.p = value.page;
    });

    this.restService.getAppUserList().subscribe( res => {
      const response = res as UserListResponse;
      if (response.status) {
        response.data.users.forEach(element => {
          this.userList.push({
            id: null,
            userId: element.id,
            name: element.name
          });
        });
      } else {
        this.toastr.error('Sorry could not fetch users list');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });

    this.multipleDropDownSettings = {
      singleSelection: false,
      idField: 'userId',
      textField: 'name',
      itemsShowLimit: 4,
      allowSearchFilter: true,
      enableCheckAll: false,
      maxHeight: 100
    };

  }

  ngOnInit() {
    this.notificationId = this.route.snapshot.paramMap.get('id') as any as number;

    this.restService.getNotificationDetails(this.notificationId).subscribe(res => {
      const response = res as NotificationDetailResponse;
      if (response.status) {
        this.model = response.data.notification;
        this.date = new Date(response.data.notification.scheduledAt);
        this.time = new Date(response.data.notification.scheduledAt);
      } else {
        this.toastr.error('Sorry could not get notification details', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  validateRequest() {
    if (this.model.title.trim().length === 0) {
      this.toastr.warning('Please enter title.');
      return false;
    } else if (this.model.message.trim().length === 0) {
      this.toastr.warning('Please enter content.');
      return false;
    } else if (this.model.users.length === 0) {
      if (!this.model.isAllSelected) {
        this.toastr.warning('Please select at least one recipient user.');
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  prepareDateTime() {
    this.model.scheduledAt  = '';
    const myDate =  new Date();

    if ( !this.sendNow ) {
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += this.date.getFullYear() + '-' + ((this.date.getMonth() + 1) + '').padStart(2, '0') + '-' + (this.date.getDate() + '').padStart(2, '0');
    } else {
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += myDate.getFullYear() + '-' + ((myDate.getMonth() + 1) + '').padStart(2, '0') + '-' + (myDate.getDate() + '').padStart(2, '0');
    }
    if (  !this.sendNow ) {
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += ' ' + (this.time.getHours() + '').padStart(2, '0') + ':' + (this.time.getMinutes() + '').padStart(2, '0') + ':' + (this.time.getSeconds() + '').padStart(2, '0');
    } else {
      // tslint:disable-next-line:max-line-length
      this.model.scheduledAt += ' ' + (myDate.getHours() + '').padStart(2, '0') + ':' + (myDate.getMinutes() + '').padStart(2, '0') + ':' + (myDate.getSeconds() + '').padStart(2, '0');
    }
  }

  submitUpdate() {
    if (this.validateRequest()) {
      this.prepareDateTime();
      this.restService.updateNotification(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('Notification updated successfully');
          this.back();
        } else {
          this.toastr.error('Sorry could not update notification', 'Error');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

  back() {
    this.location.back();
  }

}
