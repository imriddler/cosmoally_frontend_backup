import {Component, OnInit} from '@angular/core';
import {RestService} from '../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {NotificationListResponse, UserListResponse} from '../models/app.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notifications = [];
  apiLoaded = false;
  p = 1;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService,
    private router: Router,
    private toastr: ToastrService
  ) {

  }

  ngOnInit() {
    this.restService.getNotificationList().subscribe(res => {
      this.apiLoaded = true;
      const response = res as NotificationListResponse;
      if (response.status) {
        this.notifications = response.data.notifications;
      } else {
        this.toastr.error('Sorry could not fetch notifications', 'Error');
      }
    },
    error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }
}
