import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserModule } from './user/user.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { KnowledgeBoxComponent } from './knowledge-box/knowledge-box.component';
import { EditFaqComponent } from './knowledge-box/edit-faq/edit-faq.component';
import { ViewFaqComponent } from './knowledge-box/view-faq/view-faq.component';
import { AddPageComponent } from './page/add-page/add-page.component';
import { Constants } from '../app.constants';
import { AddFaqComponent } from './knowledge-box/add-faq/add-faq.component';
import { EditPageComponent } from './page/edit-page/edit-page.component';
import { ListPageComponent } from './page/list-page/list-page.component';
import { ViewPageComponent } from './page/view-page/view-page.component';
import {CategoryComponent} from './knowledge-box/category/category.component';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserAddComponent} from './user/user-add/user-add.component';
import {ProductCategoriesComponent} from './products/product-categories/product-categories.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {EditProductCategoryComponent} from './products/edit-product-category/edit-product-category.component';
import {CategorizeProductsComponent} from './products/categorize-products/categorize-products.component';
import {NotificationComponent} from './notification/notification.component';
import {ChangePasswordComponent} from './user/change-password/change-password.component';
import {UserDetailComponent} from './user/user-detail/user-detail.component';
import {AppUserListComponent} from './user-products/app-user-list/app-user-list.component';
import {ProductListComponent} from './user-products/product-list/product-list.component';
import {SendNotificationComponent} from './notification/send-notification/send-notification.component';
import {BannersComponent} from './banners/banners.component';
import {AuthGuard} from '../auth/auth-guard.service';
import {ViewNotificationComponent} from './notification/view-notification/view-notification.component';
import {ViewProductCategoryComponent} from './products/view-product-category/view-product-category.component';
import {ProductBatchListComponent} from './products/product-batch-list/product-batch-list.component';
import { BatchPreviewComponent } from './products/batch-preview/batch-preview.component';
import { ServicesComponent } from './services/services.component';
import {NotificationUsersComponent} from './notification/notification-users/notification-users.component';
import {NotificationEditComponent} from './notification/notification-edit/notification-edit.component';
import {BatchLogComponent} from './products/batch-log/batch-log.component';
import { ReadFaqListComponent } from './user-products/read-faq-list/read-faq-list.component';
import { ListEnquiryComponent } from './enquiries/list-enquiry/list-enquiry.component';
import { ViewEnquiryComponent } from './enquiries/view-enquiry/view-enquiry.component';
import { AppUpdatesComponent } from './app-updates/app-updates.component';
import { AddUpdateComponent } from './app-updates/add-update/add-update.component';

const routes: Routes =
  [
    {
      path: '',
      component: DashboardComponent,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: '/users',
          canActivate: [AuthGuard],
        },

        // Users Links
        // {
        //   path: 'users',
        //   canActivate: [],
        //   loadChildren : () => UserModule
        // },
        {
          path: 'users',
          component: UserListComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'users/add',
          component: UserAddComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'users/edit/:id',
          component: UserEditComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'users/change-password/:id',
          component: ChangePasswordComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'users/detail/:id',
          component: UserDetailComponent,
          canActivate: [AuthGuard],
        },

        // Tutorial Links
        {
          path: 'tutorials',
          component: ListPageComponent,
          canActivate: [AuthGuard],
          data: {
            pageType: Constants.PAGE_TYPE.TUTORIAL
          }
        },
        {
          path: 'tutorials/add',
          component: AddPageComponent,
          canActivate: [AuthGuard],
          data: {
            pageType: Constants.PAGE_TYPE.TUTORIAL
          }
        },
        {
          path: 'tutorials/edit/:id',
          component: EditPageComponent,
          canActivate: [AuthGuard],
          data: {
            pageType: Constants.PAGE_TYPE.TUTORIAL
          }
        },
        {
          path: 'tutorials/:id',
          component: ViewPageComponent,
          canActivate: [AuthGuard],
          data: {
            pageType: Constants.PAGE_TYPE.TUTORIAL
          }
        },

        // Product Links
        {
          path: 'products',
          component: ProductBatchListComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'products/batch/preview/:batch',
          component: BatchPreviewComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'products/batch/log/:batch',
          component: BatchLogComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'products/batch/:batchId',
          component: CategorizeProductsComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'product/categories',
          component: ProductCategoriesComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'product/categories/edit/:id',
          component: EditProductCategoryComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'product/categories/view/:id',
          component: ViewProductCategoryComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'app-users',
          component: AppUserListComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'app-users/faqs/:id',
          component: ReadFaqListComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'app-users/:id',
          component: ProductListComponent,
          canActivate: [AuthGuard],
        },

        // Service Links
        // {
        //   path: 'services',
        //   component: ListPageComponent,
        //   data: {
        //     pageType: Constants.PAGE_TYPE.SERVICE
        //   }
        // }
        {
          path: 'services',
          component: ServicesComponent,
          canActivate: [AuthGuard]
        },

        // FAQ Links
        {
          path: 'faqs',
          component: KnowledgeBoxComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'faqs/add',
          component: AddFaqComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'faqs/edit/:id',
          component: EditFaqComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'faqs/categories',
          component: CategoryComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'faqs/:id',
          component: ViewFaqComponent,
          canActivate: [AuthGuard],
        },

        // Banner Links
        {
          path: 'banners',
          component: BannersComponent,
          canActivate: [AuthGuard],
        },

        // Notification Links
        {
          path : 'notifications',
          component : NotificationComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'notifications/send',
          component: SendNotificationComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'notifications/view/:id',
          component: ViewNotificationComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'notifications/users/:id',
          component: NotificationUsersComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'notifications/edit/:id',
          component: NotificationEditComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'enquiries',
          component : ListEnquiryComponent,
          canActivate: [AuthGuard]
        },
        {
          path : 'enquiries/view/:id',
          component: ViewEnquiryComponent,
          canActivate : [AuthGuard]
        },
        {
          path: 'app-updates',
          component: AppUpdatesComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'add-update',
          component: AddUpdateComponent,
          canActivate: [AuthGuard]
        }
      ]
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
