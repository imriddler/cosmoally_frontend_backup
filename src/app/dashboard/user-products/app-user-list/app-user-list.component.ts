import {Component, OnInit, TemplateRef} from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {UserListResponse} from '../../models/app.model';
import { BaseResponse } from '../../../base-response';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-app-user-list',
  templateUrl: './app-user-list.component.html',
  styleUrls: ['./app-user-list.component.scss']
})
export class AppUserListComponent implements OnInit {

  users = [];
  apiLoaded = false;
  modalRef: BsModalRef;
  faqRead = null as number;
  p = 1;
  faqLimitModel = {
    faqLimit : null as number,
    userId : null as number
  };

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';
  public popoverBlockMessage = 'Are you really sure you want to block this user?';


  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private modal: BsModalService
  ) { }

  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this.restService.getAppUserList().subscribe(res => {
      const response = res as UserListResponse;
      if (response.status) {
        this.users = response.data.users;
        this.apiLoaded = true;
      } else {
        this.toastr.error('Sorry could not fetch users.', 'Error');
        this.apiLoaded = true;
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  delete(id) {
    this.restService.deleteAppUser(id).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('App user deleted successfully');
        this.getUserList();
      } else {
        this.toastr.error('Sorry could not delete app user', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  block(id) {
    this.restService.blockUser(id).subscribe( res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('User blocked successfully');
        this.getUserList();
      } else {
        this.toastr.error('Sorry could not block user', 'Error');
      }
    } , error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }


  unblock(id) {
    this.restService.unBlockUser(id).subscribe( res => {
      const response = res as BaseResponse;
      if (response) {
        this.toastr.success('User unblocked successfully');
        this.getUserList();
      } else {
        this.toastr.error('Sorry could not unblock user', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

  openEditFaqLimitModal(template: TemplateRef<any>, faqLimit, userId, faqRead) {
    this.faqLimitModel = {
      faqLimit : faqLimit,
      userId: userId
    };
    this.faqRead = faqRead;
    this.modalRef = this.modal.show(template);
  }

  hideModal() {
    this.faqLimitModel = {
      faqLimit : null as number,
      userId : null as number
    };
    this.modalRef.hide();
  }

  updateMaxFaqRead() {
    if (this.faqLimitModel.faqLimit == 0) {
        this.toastr.warning('Faq limit can not be zero');
        return false;
    } else if (this.faqLimitModel.faqLimit < this.faqRead) {
        this.toastr.warning('Faq limit can not be less than faq read.');
        return false;
    } else {
    this.restService.updateMaxFaqRead(this.faqLimitModel).subscribe(res => {
      this.faqLimitModel = {
        faqLimit : null as number,
        userId : null as number
      };
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Max faq read updated successfully');
        this.getUserList();
        this.hideModal();
      } else {
        this.toastr.error('Sorry could not update max faq read', 'Error');
      }
    }, error => {
      this.faqLimitModel = {
        faqLimit : null as number,
        userId : null as number
      };
      this.toastr.error(error.error.message, 'Oops');
    });
}
  }

}
