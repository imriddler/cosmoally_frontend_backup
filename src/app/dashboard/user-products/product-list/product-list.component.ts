import { Component, OnInit } from '@angular/core';
import {UserModel, UserViewResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  user: UserModel = new UserModel();

  userId = null as number;
  p = 1;
  apiLoaded = false;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe(value => {
      this.userId = value.get('id') as any as number;
    });
  }

  ngOnInit() {
    this.restService.getUserDetails(this.userId).subscribe(res => {
      const response = res as UserViewResponse;
      this.apiLoaded = true;
      if (response.status) {
        this.user = response.data.users;
      } else {
        this.toastr.error('Sorry could not fetch user details', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

}
