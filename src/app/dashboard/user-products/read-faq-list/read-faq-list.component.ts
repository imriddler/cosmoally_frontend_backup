import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserFaqReadListResponse } from '../../models/app.model';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../rest.service';

@Component({
  selector: 'app-read-faq-list',
  templateUrl: './read-faq-list.component.html',
  styleUrls: ['./read-faq-list.component.scss']
})
export class ReadFaqListComponent implements OnInit {

  
  faqList = [];
  userId = null as number;
  user = null;
  p = 1;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute
    ) {
      this.userId = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.getFaqs();
  }

  getFaqs() {
    this.restService.getUserFaqReadList(this.userId).subscribe(res => {
      const response = res as UserFaqReadListResponse;
      if (response.status) {
        this.user = response.data.user;
        this.faqList = response.data.faqs;
        this.faqList.map((item) => {
          item.faq.faqTypes = item.faq.faqTypes.map((ele) => {
            return ele.name;
          });
        });
      } else {
        this.toastr.error('Sorry could not fetch the faq list.', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }


}
