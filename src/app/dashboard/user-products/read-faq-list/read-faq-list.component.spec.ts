import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadFaqListComponent } from './read-faq-list.component';

describe('ReadFaqListComponent', () => {
  let component: ReadFaqListComponent;
  let fixture: ComponentFixture<ReadFaqListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadFaqListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadFaqListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
