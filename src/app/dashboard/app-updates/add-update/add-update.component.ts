import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../rest.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponse } from '../../../base-response';

@Component({
  selector: 'app-add-update',
  templateUrl: './add-update.component.html',
  styleUrls: ['./add-update.component.scss']
})
export class AddUpdateComponent implements OnInit {


  model = {
    platform: '',
    version: '',
    live_at: '',
    update_required: true,
    update_text: ''
  };

  live_now = true;
  date = new Date();
  time = new Date();

  p=1;

  minDate = new Date();

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }



  validateRequest() {
    if(this.model.platform.length == 0) {
      this.toastr.warning('Please select platform');
      return false;
    } else if (this.model.version.length  == 0) {
      this.toastr.warning('Please enter version');
      return false;
    } else if (this.model.version.split('.').length != 3){
      this.toastr.warning('App Version is not valid');
      return false;
    } else {
      return true; 
    }
  }


  prepareDateTime() {
    this.model.live_at = '';
    const myDate = new Date();
    if (!this.live_now) {
      if (this.date !== null) {
        // tslint:disable-next-line:max-line-length
        this.model.live_at += this.date.getFullYear() + '-' + ((this.date.getMonth() + 1) + '').padStart(2, '0') + '-' + (this.date.getDate() + '').padStart(2, '0');
      } else {
        // tslint:disable-next-line:max-line-length
        this.model.live_at += myDate.getFullYear() + '-' + ((myDate.getMonth() + 1) + '').padStart(2, '0') + '-' + (myDate.getDate() + '').padStart(2, '0');
      }
      if (this.time !== null) {
        // tslint:disable-next-line:max-line-length
        this.model.live_at += ' ' + (this.time.getHours() + '').padStart(2, '0') + ':' + (this.time.getMinutes() + '').padStart(2, '0') + ':' + (this.time.getSeconds() + '').padStart(2, '0');
      } else {
        // tslint:disable-next-line:max-line-length
        this.model.live_at += ' ' + (myDate.getHours() + '').padStart(2, '0') + ':' + (myDate.getMinutes() + '').padStart(2, '0') + ':' + (myDate.getSeconds() + '').padStart(2, '0');
      }
    } else {
      // tslint:disable-next-line:max-line-length
      this.model.live_at += myDate.getFullYear() + '-' + ((myDate.getMonth() + 1) + '').padStart(2, '0') + '-' + (myDate.getDate() + '').padStart(2, '0');
      // tslint:disable-next-line:max-line-length
      this.model.live_at += ' ' + (myDate.getHours() + '').padStart(2, '0') + ':' + (myDate.getMinutes() + '').padStart(2, '0') + ':' + (myDate.getSeconds() + '').padStart(2, '0');
    }
  }


  addUpdate() {
    if (this.validateRequest()) {
      this.prepareDateTime();
      this.restService.addUpdate(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('App Update added successfully.');
          this.router.navigate(['/app-updates'], {
            queryParams: {page: this.p}
          });
        } else {
          this.toastr.error('Sorry could not add App Update.');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Error');
      });
    }
  }

}
