import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';
import { ToastrService } from 'ngx-toastr';
import { AppUpdate, AppUpdateResponse } from '../models/app.model';
import { BaseResponse } from '../../base-response';

@Component({
  selector: 'app-app-updates',
  templateUrl: './app-updates.component.html',
  styleUrls: ['./app-updates.component.scss']
})
export class AppUpdatesComponent implements OnInit {

  updates = [];
  apiLoaded = false;
  p = 1;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService,
    private toastr: ToastrService
  ) {
  }


  ngOnInit() {
    this.getUpdateList();
  }

  getUpdateList() {
    this.restService.getAppUpdateList().subscribe(res => {
      const response = res as AppUpdateResponse;
      this.apiLoaded = true;
      if (response.status) {
        this.updates = response.data.updates as AppUpdate[];
      } else {
        this.toastr.error('Could not fetch updates.');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }


  delete(id){
    this.restService.deleteUpdate(id).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Update deleted successfully');
        this.getUpdateList();
      } else {
        this.toastr.error('Sorry could not delete update');
      }
    }, error => {
      if(error && error.error && error.error.message) {
        this.toastr.error(error.error.message);
      } else {
        this.toastr.error('Something went wrong');
      }
    });
  }
}
