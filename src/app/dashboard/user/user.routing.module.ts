import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {UserAddComponent} from './user-add/user-add.component';
import {UserComponent} from './user/user.component';
import {UserEditComponent} from './user-edit/user-edit.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {ChangePasswordComponent} from './change-password/change-password.component';

const routes: Routes =
  [
    // {
    //   path: '',
    //   component: UserComponent,
    //   children: [
    //     {
    //       path: '',
    //       canActivate: [],
    //       component: UserListComponent
    //     },
    //     {
    //       path: 'add',
    //       canActivate: [],
    //       component: UserAddComponent
    //     },
    //     {
    //       path: 'edit/:id',
    //       component: UserEditComponent
    //     },
    //     {
    //       path: 'change-password/:id',
    //       component: ChangePasswordComponent
    //     },
    //     {
    //       path: 'detail/:id',
    //       canActivate: [],
    //       component: UserDetailComponent
    //     },
    //   ]
    // }
  ];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
