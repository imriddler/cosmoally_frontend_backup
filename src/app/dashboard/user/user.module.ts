import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserComponent} from './user/user.component';
import {UserRoutingModule} from './user.routing.module';
import {UserService} from './user.service';
import {FormsModule} from '@angular/forms';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';

@NgModule({
  declarations: [
    UserComponent,
    // UserAddComponent,
    // UserListComponent,
    // UserEditComponent,
    // UserDetailComponent,
    // ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'warning' // set defaults here
    })
  ],
  providers: [
    UserService
  ]
})
export class UserModule {
}
