import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../user.service';
import {RestService} from '../../../rest.service';
import {Page, PageResponse, UserListResponse, UserModel} from '../../models/app.model';
import {BaseResponse} from '../../../base-response';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  apiLoaded = false;
  users: UserModel[] = [];
  p = 1;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.getUsersList();
  }

  getUsersList() {
    this.restService.getUserList().subscribe(res => {
        const response = res as UserListResponse;
        this.users = response.data.users as UserModel[];
        this.apiLoaded = true;
      },
      errRes => {
        this.apiLoaded = true;
        if (errRes && errRes.error && errRes.error.message) {
          this.toastr.error(errRes.error.message, 'Oops!');
        } else {
          this.toastr.error('Unable to complete your request, please try after sometime.', 'Opps!');
        }
      });
  }

  delete(id: number) {
      this.restService.deleteUser(id).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.toastr.success('User deleted successfully');
          this.getUsersList();
        } else {
          this.toastr.warning('Sorry Could not Create User', 'Oops');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }


}
