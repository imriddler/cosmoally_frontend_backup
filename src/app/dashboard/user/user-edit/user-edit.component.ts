import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseResponse} from '../../../base-response';
import {UserViewResponse, UserModel} from '../../models/app.model';
import { AuthService } from '../../../../app/auth/auth.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  model = {
    id: null as number,
    name: '',
    email: '',
    password: '',
    oldPassword: ''
  };

  userId = null;
  user: UserModel;

  p = null as number;

  apiLoaded = false;

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location,
    private router: Router,
    private authService: AuthService
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });

    this.userId = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.restService.getUserDetails(this.userId as any as number).subscribe(res => {
      const response = res as UserViewResponse;
      this.user = response.data.users as UserModel;

      this.model.id = this.user.id;
      this.model.name = this.user.name;
      this.model.email = this.user.email;

      this.apiLoaded = true;
    });
  }

  UpdateUser() {
    this.restService.editUser(this.model).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Admin user updated successfully');
        if(this.authService.getUserId() == String(this.model.id)){
          this.authService.setUserName(this.model.name);
        }
        this.router.navigate(['/users']);
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Oops!');
      }
    }, errRes => {
      if (errRes && errRes.error && errRes.error.message) {
        this.toastr.error(errRes.error.message, 'Oops!');
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Opps!');
      }
    });
  }

  validateRequest() {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.model.name === '') {
      this.toastr.warning('Name is required');
      return false;
    } else if (this.model.name.length > 200) {
      this.toastr.warning('Name length should be less than 200 characters');
      return false;
    } else if (this.model.email === '') {
      this.toastr.warning('Email is required');
      return false;
    } else if (!emailRegex.test(String(this.model.email).toLowerCase())) {
      this.toastr.warning('Please enter valid email address');
      return false;
    } else if (this.model.email.length > 200) {
      this.toastr.warning('Email length should be less than 200 characters');
      return false;
    } else {
      return true;
    }
  }

  submit() {
    if (this.validateRequest()) {
      this.UpdateUser();
    }
  }

}
