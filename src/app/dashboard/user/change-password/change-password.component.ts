import {Component, OnInit} from '@angular/core';
import {UserModel, UserViewResponse} from '../../models/app.model';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {BaseResponse} from '../../../base-response';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  model = {
    id: null as number,
    newPassword: '',
    confirmPassword: ''
  };

  isPasswordViewed = false;
  isConfirmViewed = false;

  user: UserModel;

  apiLoaded = false;

  p =  null as number;


  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.model.id = route.snapshot.paramMap.get('id') as any as number;
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }

  ngOnInit() {
  }

  changePassword() {
    this.restService.changePassword(this.model).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Admin user password updated successfully');
        this.router.navigate(['/users'], {queryParams: { page: this.p }});
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Oops!');
      }
    }, errRes => {
      if (errRes && errRes.error && errRes.error.message) {
        this.toastr.error(errRes.error.message, 'Oops!');
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Opps!');
      }
    });
  }

  validateRequest() {
    if (this.model.newPassword === '') {
      this.toastr.warning('New Password is required');
      return false;
    } else if (this.model.confirmPassword === '') {
      this.toastr.warning('Confirm Password is required');
      return false;
    } else if (this.model.newPassword.length < 6) {
      this.toastr.warning('New password length should be greater than 6 characters');
      return false;
    } else if (this.model.newPassword.length > 20) {
      this.toastr.warning('Password length should be less than 20 characters');
      return false;
    } else if (this.model.newPassword !== this.model.confirmPassword) {
      this.toastr.warning('Passwords didn\'t match');
      return false;
    } else {
      return true;
    }
  }

  submit() {
    if (this.validateRequest()) {
      this.changePassword();
    }
  }

  viewPasswordToggle() {
    this.isPasswordViewed = !this.isPasswordViewed;
  }

  viewConfirmToggle() {
    this.isConfirmViewed = !this.isConfirmViewed;
  }
}
