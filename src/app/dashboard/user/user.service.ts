import 'rxjs/add/operator/map';

import {HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../auth/auth.service';
import {ApiEndPoints, Configuration} from '../../app.constants';

@Injectable()
export class UserService {

  constructor(
    private _http: HttpClient,
    private _apiEndPoints: ApiEndPoints,
    private _configuration: Configuration,
    private _authService: AuthService
  ) {

  }

  public getUsers<T>(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    };
    return this._http.get<any>(this._apiEndPoints.USER_LIST, httpOptions);
  }


  public add<T>(model: any): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    };
    return this._http.post<any>(this._apiEndPoints.USER_ADD, model, httpOptions);

  }

}
