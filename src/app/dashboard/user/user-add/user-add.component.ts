import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {RestService} from '../../../rest.service';
import {BaseResponse} from '../../../base-response';
import {ActivatedRoute, Router} from '@angular/router';
import {UserAddModel} from '../../models/app.model';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  model = {
    name: '',
    email: '',
    password: ''
  };

  isPasswordViewed = false;

  p = null as number;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
  }

  ngOnInit() {

  }

  addNewUser() {
    this.restService.addUser(this.model).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Admin user added successfully');
        this.router.navigate(['/users']);
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Oops!');
      }
    }, errRes => {
      if (errRes && errRes.error && errRes.error.message) {
        this.toastr.error(errRes.error.message, 'Oops!');
      } else {
        this.toastr.error('Unable to complete your request, please try after sometime.', 'Opps!');
      }
    });
  }

  validateRequest() {
    // tslint:disable-next-line:max-line-length
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.model.name === '') {
      this.toastr.warning('Name is required');
      return false;
    } else if (this.model.name.length > 200) {
      this.toastr.warning('Name length should be less than 200 characters');
      return false;
    } else if (this.model.email === '') {
      this.toastr.warning('Email is required');
      return false;
    } else if (!emailRegex.test(String(this.model.email).toLowerCase())) {
      this.toastr.warning('Please enter valid email address');
      return false;
    } else if (this.model.email.length > 200) {
      this.toastr.warning('Email length should be less than 200 characters');
      return false;
    } else if (this.model.password === '') {
      this.toastr.warning('Password is required');
      return false;
    } else if (this.model.password.length < 6) {
      this.toastr.warning('Password length should be greater than 6 characters');
      return false;
    } else if (this.model.password.length > 20) {
      this.toastr.warning('Password length should be less than 20 characters');
      return false;
    } else {
      return true;
    }
  }

  submit() {
    if (this.validateRequest()) {
      this.addNewUser();
    }
  }

  viewPasswordToggle() {
    this.isPasswordViewed = !this.isPasswordViewed;
  }
}
