import { Component, OnInit } from '@angular/core';
import {UserModel, UserViewResponse} from '../../models/app.model';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  userId = null;

  user: UserModel;

  apiLoaded = false;

  p = null as number;

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastService: ToastrService,
    private router: Router,
  ) {
    route.queryParams.subscribe( value => {
      this.p = value.page;
    });
    this.userId = this.route.snapshot.paramMap.get('id') as any as number;
  }

  ngOnInit() {
    this.restService.getUserDetails(this.userId as any as number).subscribe(res => {
      const response = res as UserViewResponse;
      this.user = response.data.users as UserModel;
      this.apiLoaded = true;
    });
  }

}
