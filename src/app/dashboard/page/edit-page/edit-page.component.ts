import {Component, OnInit} from '@angular/core';
import {UploadedImage, ImagesResultModel, ImageModel, PageDetailResponse, Page} from '../../models/app.model';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {BaseResponse} from '../../../base-response';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit {
  model = {
    pageType: null as number,
    id: null as number,
    title: '',
    content: '',
    sortOrder: null as number,
    images: [],
    data: []
  };

  p = null as number;

  uploadedImages: UploadedImage[] = [];
  selected_image = '';

  pageTitle = '';
  editorConfig = {};

  constructor(
    private route: ActivatedRoute,
    public restService: RestService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location) {

    this.model.id = this.route.snapshot.paramMap.get('id') as any as number;

    this.route.data.subscribe(value => {
      this.model.pageType = value.pageType;
    });

    this.route.queryParams.subscribe(value => {
      this.p = value.page;
    });

    if (this.model.pageType === 1) {
      this.pageTitle = 'Tutorials';
    } else if (this.model.pageType === 2) {
      this.pageTitle = 'Services';
    }
  }

  ngOnInit() {
    this.restService.getPageDetail(this.model.id as any as number).subscribe(res => {
      const response = res as PageDetailResponse;
      this.model.content = response.data.page.content;
      this.model.images = response.data.page.images;
      this.model.sortOrder = response.data.page.sortOrder;
      this.model.title = response.data.page.title;
      this.model.data = response.data.page.data;
      console.log(this.model);
    });

    this.editorConfig = {
      'editable': true,
      'spellcheck': true,
      'height': '300px',
      'minHeight': '100px',
      'translate': 'no',
      'uploadUrl': this.restService.apiUrl + 'image/upload'
    };
  }

  readURL(event) {
    if (event.target.files) {
      const filesToUpload: File[] = [];
      for (let i = 0; i < event.target.files.length; i++) {
        filesToUpload.push(event.target.files[i]);
      }
      this.restService.uploadImages(filesToUpload).subscribe(res => {
        const response = res as ImagesResultModel;
        if (response.status) {
          response.data.images.forEach((element: UploadedImage) => {
            this.uploadedImages.push(element);
            this.model.images.push({
              id: null,
              imageType: 1,
              imageUrl: element.fullUrl
            });
          });
          console.log(this.selected_image);
          this.selected_image = '';
        }
      });
    }
  }

  deleteImage(imageUrl: string) {
    let modelImageIndex;
    let image: ImageModel;
    this.model.images.forEach((element: ImageModel, ind: number) => {
      if (element.imageUrl === imageUrl) {
        modelImageIndex = ind;
        image = element;
        return;
      }
    });

    if (image.id == null) {
      const uploadedImage = this.uploadedImages.find((element: UploadedImage) => {
        if (element.fullUrl === image.imageUrl) {
          return true;
        }
      });
      this.restService.deleteImage(uploadedImage.id).subscribe(res => {
        const response = res as ImagesResultModel;
        if (response.status) {
          const index = this.uploadedImages.findIndex((element: UploadedImage) => {
            if (element.id === uploadedImage.id) {
              return true;
            }
          });
          this.uploadedImages.splice(index, 1);
          this.model.images = this.model.images.filter((element: ImageModel) => {
            if (element.imageUrl !== uploadedImage.fullUrl) {
              return true;
            }
          });
        } else {
          this.toastr.error('Could not delete image');
        }
      });
    } else {

      this.restService.deletePageImage(image.id).subscribe(res => {
        const response = res as ImagesResultModel;
        if (response.status) {
          this.model.images.splice(modelImageIndex, 1);
        } else {
          this.toastr.error('Could not delete image');
        }
      });
    }


  }

  validateRequest() {
    if (this.model.title.trim().length === 0) {
      this.toastr.warning('Please enter title.');
      return false;
    } else if (this.model.title.length > 100) {
      this.toastr.warning('Title should be less than 100 characters.');
      return false;
    } else if (this.model.sortOrder == null) {
      this.toastr.warning('Please enter sort order');
      return false;
    } else if (this.model.sortOrder == 0) {
      this.toastr.warning('Sort order can not be zero');
      return false;
    } else if (this.model.content.trim().length === 0) {
      this.toastr.warning('Please enter content.');
      return false;
    } else if (this.model.pageType === 1 && this.model.data.length > 0) {
      if (this.dataValidation()) {
        this.toastr.warning('Question and answer can\'t be empty.');
      } else {
        return true;
      }
    } else {
      return true;
    }
  }


  submit() {
    console.log(this.model);
    if (this.validateRequest()) {
      this.restService.submitPage(this.model, this.model.id).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          switch (this.model.pageType) {
            case 1:
              this.router.navigateByUrl('/tutorials');
              this.toastr.success('Tutorial updated successfully.');
              break;
            case 2:
              this.router.navigateByUrl('/services');
              break;
            default:
              this.toastr.warning('Cannot go back');
          }
        } else {
          this.toastr.error('Something went wrong.', 'Oops');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

  dataValidation(): boolean {
    return this.model.data.some(item => {
      return (item.question.trim().length === 0 || item.answer.trim().length === 0);
    });
  }

  addMore() {
    this.model.data.push({
      id: null,
      question: '',
      answer: ''
    });
  }

  removeData(index: number) {
    this.model.data.splice(index, 1);
  }

}
