import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../rest.service';
import { PageDetailResponse, Page } from '../../models/app.model';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

// @ts-ignore
@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit {

  pageTitle: string;
  pageType: number;
  id: number;

  page: Page;
  p = null as number;

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.route.data.subscribe(value => {
      this.pageType = value.pageType as number;
    });

    switch (this.pageType) {
      case 1:
        this.pageTitle = 'Tutorial';
        break;
      case 2:
        this.pageTitle = 'Service';
        break;
      default:
    }

    this.id = this.route.snapshot.paramMap.get('id') as any as number;
    this.route.queryParams.subscribe(value => {
      this.p = value.page;
    });
  }

  ngOnInit() {

    this.restService.getPageDetail(this.id).subscribe(res => {
      const response = res as PageDetailResponse;
      if (response.status) {
        this.page = response.data.page as Page;
        console.log(this.page);
      } else {
        this.toastr.error('Could not fetch ' + this.pageTitle + ' Details');
      }
    });
  }

}
