import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Page, PageResponse} from '../../models/app.model';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {

  pageList: Page[] = [];
  apiLoaded = false;
  p = 1;
  pageTitle = '';
  pageType: number = 0 as null;

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private route: ActivatedRoute,
    private restService: RestService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.route.data.subscribe(value => {
      this.pageType = value.pageType as number;
    });

    this.route.queryParams.subscribe(value =>  {
      this.p = value.page ? value.page : 1;
    });

    if (this.pageType === 1) {
      this.pageTitle = 'Tutorials';
    } else if (this.pageType === 2) {
      this.pageTitle = 'Services';
    }
  }

  ngOnInit() {
    this.restService.getPageList(this.pageType).subscribe(res => {
      const response = res as PageResponse;
      if (response.status) {
        this.pageList = response.data.pages;
        this.apiLoaded = true;
      } else {
        this.toastr.error('Could not fetch data');
        this.apiLoaded = true;
      }
    });
  }

  delete(id) {
      this.restService.deletePage(id).subscribe(res => {
        const response = res as PageResponse;
        if (response.status) {
          const index = this.pageList.findIndex((element: Page) => {
            if (element.id === id) {
              return true;
            }
          });
          this.pageList.splice(index, 1);
        }
      });
  }

}
