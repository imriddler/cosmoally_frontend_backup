import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {BaseResponse} from '../../../base-response';
import {Location} from '@angular/common';
import {UploadedImage, ImageModel, ImagesResultModel} from '../../models/app.model';

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.scss']
})
export class AddPageComponent implements OnInit {

  model = {
    pageType: 0 as null,
    id: '',
    title: '',
    content: '',
    sortOrder: null as number,
    images: [],
    data: []
  };

  p = null as number;

  editorConfig = {};
  uploadedImages: UploadedImage[] = [];
  selected_image = '';

  pageTitle = '';

  constructor(
    private route: ActivatedRoute,
    public restService: RestService,
    private toastr: ToastrService,
    private router: Router) {

    this.route.queryParams.subscribe(value => {
      this.p = value.page;
    });

    this.route.data.subscribe(value => {
      this.model.pageType = value.pageType;
    });

    if (this.model.pageType == 1) {
      this.pageTitle = 'Tutorials';
    } else if (this.model.pageType == 2) {
      this.pageTitle = 'Services';
    }
  }

  ngOnInit() {
    this.editorConfig = {
      'editable': true,
      'spellcheck': true,
      'height': '300px',
      'minHeight': '100px',
      'translate': 'no',
      'uploadUrl': this.restService.apiUrl + 'image/upload'
    };
  }

  readURL(event) {
    if (event.target.files) {
      const filesToUpload: File[] = [];
      for (let i = 0; i < event.target.files.length; i++) {
        filesToUpload.push(event.target.files[i]);
      }
      this.restService.uploadImages(filesToUpload).subscribe(res => {
        const response = res as ImagesResultModel;
        if (response.status) {
          response.data.images.forEach((element: UploadedImage) => {
            this.uploadedImages.push(element);
            this.model.images.push({
              id: element.id,
              imageType: 1,
              imageUrl: element.url
            });
          });
          this.selected_image = '';
        }
      });
    }
  }

  deleteImage(id: Number) {
    this.restService.deleteImage(id).subscribe(res => {
      const response = res as ImagesResultModel;
      if (response.status) {
        let url = '';
        const index = this.uploadedImages.findIndex((element: UploadedImage) => {
          if (element.id === id) {
            url = element.url;
            return true;
          }
        });
        this.uploadedImages.splice(index, 1);
        this.model.images = this.model.images.filter((element: ImageModel) => {
          if (element.imageUrl != url) {
            return true;
          }
        });
      } else {
        this.toastr.error('Could not delete image');
      }
    });
  }

  validateRequest() {
    if (this.model.title.trim().length === 0) {
      this.toastr.warning('Please enter title');
      return false;
    } else if (this.model.title.length > 100) {
      this.toastr.warning('Title should be less than 100 characters.');
      return false;
    } else if (this.model.sortOrder == null) {
      this.toastr.warning('Please enter sort order');
      return false;
    } else if (this.model.sortOrder == 0) {
      this.toastr.warning('Sort order can not be zero');
      return false;
    } else if (this.model.content.trim().length === 0) {
      this.toastr.warning('Please enter content.');
      return false;
    } else if (this.model.pageType === 1 && this.model.data.length > 0) {
      if (this.dataValidation()) {
        this.toastr.warning('Question and answer can\'t be empty.');
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  submit() {
    if (this.validateRequest()) {
      this.restService.submitPage(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          switch (this.model.pageType) {
            case 1:
              this.router.navigateByUrl('/tutorials');
              this.toastr.success('Tutorial added successfully.');
              break;
            case 2:
              this.router.navigateByUrl('/services');
              break;
            default:
              this.toastr.warning('Cannot go back.');
          }
        } else {
          this.toastr.error('Something went wrong.');
        }
      }, error => {
        this.toastr.error(error.error.message);
      });
    }
  }

  addMore() {
    this.model.data.push({
      id: null,
      question: '',
      answer: ''
    });
  }

  removeData(index: number) {
    this.model.data.splice(index, 1);
  }

  dataValidation(): boolean {
    return this.model.data.some(item => {
      return (item.question.trim().length === 0 || item.answer.trim().length === 0);
    });
  }
}
