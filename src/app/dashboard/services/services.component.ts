import {Component, OnInit, TemplateRef} from '@angular/core';
import {RestService} from '../../rest.service';
import {ToastrService} from 'ngx-toastr';
import {ServiceListResponse, ServiceDetailResponse, Service} from '../models/app.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {BaseResponse} from '../../base-response';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  listone: Service[] = [];
  listtwo: Service[] = [];

  p1 = 1;
  p2 = 1;

  modalRef: BsModalRef;

  model = {
    id: null as number,
    name: '',
    listType: null as number,
    sortOrder: null as number
  };

  public popoverTitle = 'Are you sure?';
  public popoverMessage = 'Are you really sure you want to delete this?';

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private modal: BsModalService
  ) {
  }

  ngOnInit() {
    this.refreshServiceList();
  }

  refreshServiceList() {
    // Fetching First List Services
    this.restService.getServiceList(1).subscribe(res => {
      const response = res as ServiceListResponse;
      if (response.status) {
        this.listone = response.data.serviceList;
      } else {
        this.toastr.error('Sorry could not fetch services', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message);
    });


    // Fething Second List Services
    this.restService.getServiceList(2).subscribe(res => {
      const response = res as ServiceListResponse;
      if (response.status) {
        this.listtwo = response.data.serviceList;
      } else {
        this.toastr.error('Sorry could not fetch services', 'Error');
      }
    }, error => {
      this.toastr.error(error.message);
    });
  }

  getServiceDetail(id) {
    this.restService.getServiceDetail(id).subscribe(res => {
      const response = res as ServiceDetailResponse;
      if (response.status) {
        this.model = response.data.service;
      } else {
        this.toastr.error('Sorry could not get service detail', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }


  openAddServiceModal(template: TemplateRef<any>) {
    this.model = {
      id: null as number,
      name: '',
      listType: null as number,
      sortOrder: null as number
    };
    this.modalRef = this.modal.show(template);
  }

  openEditServiceModal(template: TemplateRef<any>, id) {
    this.getServiceDetail(id);
    this.modalRef = this.modal.show(template);
  }

  hideModal() {
    this.modalRef.hide();
  }

  validateRequest() {
    const regex = new RegExp('^\\d+$');
    if (this.model.listType == null || this.model.listType == 0) {
      this.toastr.warning('Please select service list');
      return false;
    } else if (this.model.name.trim().length === 0) {
      this.toastr.warning('Please enter service name.');
      return false;
    } else if (regex.test(this.model.name)) {
      this.toastr.warning('Service name should not be a number');
      return false;
    } else if (this.model.name.length > 250) {
      this.toastr.warning('Service name can not be longer than 250 characters');
    } else if (this.model.sortOrder == null) {
      this.toastr.warning('Please enter sort order');
      return false;
    }  else if (!regex.test(String(this.model.sortOrder))) {
      this.toastr.warning('Sort order should be a number');
      return false;
    } else if (this.model.sortOrder === 0) {
      this.toastr.warning('Sort order can not be zero');
      return false;
    } else {
      return true;
    }
  }

  submitAdd() {
    if (this.validateRequest()) {
      this.restService.addService(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.hideModal();
          this.toastr.success('Service added successfully');
          this.refreshServiceList();
        } else {
          this.toastr.error('Sorry could not add service', 'Error');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

  submitUpdate() {
    if (this.validateRequest()) {
      this.restService.updateService(this.model).subscribe(res => {
        const response = res as BaseResponse;
        if (response.status) {
          this.hideModal();
          this.toastr.success('Service updated successfully');
          this.refreshServiceList();
        } else {
          this.toastr.error('Sorry could not update service', 'Error');
        }
      }, error => {
        this.toastr.error(error.error.message, 'Oops');
      });
    }
  }

  deleteService(id) {
    this.restService.deleteService(id).subscribe(res => {
      const response = res as BaseResponse;
      if (response.status) {
        this.toastr.success('Service deleted successfully');
        this.refreshServiceList();
      } else {
        this.toastr.error('Sorry could not delete service', 'Error');
      }
    }, error => {
      this.toastr.error(error.error.message, 'Oops');
    });
  }

}
