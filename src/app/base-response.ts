export class  BaseResponse {
    status: boolean;
    message: string;
    type: string;
    httpStatusCode: number;
}