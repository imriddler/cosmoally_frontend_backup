import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  // public SERVER_URL = 'http://localhost:8000';
  // public SERVER_URL = 'http://api.cosmoally.demoexample.xyz';
  public SERVER_URL = 'http://api.cosmoally.io';
  public API_URL = '/api/v1/';
  public OAUTH_CLIENT_ID = 3;
  public OAUTH_CLIENT_SECRET = 'KQsQ1GO2zEuGOrv5ivOGr2LGywuhSy3Uq1h3Za8N';
  public SERVER_API_URL = this.SERVER_URL + this.API_URL;
}

@Injectable()
export class Constants {
  public static PAGE_TYPE = {
    TUTORIAL: 1,
    SERVICE: 2,
    ABOUT_US: 3,
    TERMS_AND_CONDITIONS: 4
  };

  public static PAGE_TITLE = {
    TUTORIALS_TEXT: 'Tutorials',
    SERVICES_TEXT: 'Services',
    ABOUT_US_TEXT: 'About us',
    TERMS_AND_CONDITIONS_TEXT: 'Terms and Conditions'
  };
}

@Injectable()
export class ApiEndPoints {

  public LOGIN_REQUEST;
  public SIGNUP_REQUEST;
  public PASSWORD_RESET_REQUEST;
  public PASSWORD_RESET_VERIFICATION;
  public PASSWORD_RESET_SUBMIT;

  // project related Api end points
  public USER_LIST;
  public USER_ADD;

  public PROJECT_LIST;
  public PROJECT_DETAIL;
  public PROJECT_OVERVIEW;
  public PROJECT_DASHBOARD_STATS;
  public PROJECT_PLATFORM_DETAIL;
  public PROJECT_DEVICE_LIST;
  public PROJECT_NOTIFICATION_SUBMIT;
  public ANDROID_PERMISSIONS_LIST;
  public TIMEZONE_LIST;
  public ADD_NEW_PROJECT;
  public ADD_PRODUCT_TO_PROJECT;
  public UPDATE_PLATFORM;
  public PLATFORM_LIST;
  public LIST_PRODUCTS_AVAILABLE;
  public LIST_PRODUCTS_ENABLED;
  public LIST_PROJECT_SEGMENTS;

  public DEVICE_DETAIL;
  public DEVICE_TESTING_MARK;
  public DEVICE_TESTING_SETTING;
  public ADD_NEW_PLATFORM: string;
  public UPLOAD_IMAGE_URL: string;



  constructor(private configuration: Configuration) {

    // Login URL
    this.LOGIN_REQUEST = this.configuration.SERVER_API_URL + 'backend/user/login';
    this.SIGNUP_REQUEST = this.configuration.SERVER_API_URL + '/user/join';


    this.PASSWORD_RESET_REQUEST = this.configuration.SERVER_API_URL + '/password/request';
    this.PASSWORD_RESET_VERIFICATION = this.configuration.SERVER_API_URL + '/password/token/verify';
    this.PASSWORD_RESET_SUBMIT = this.configuration.SERVER_API_URL + '/password/reset';

    // User API EndPoints
    this.USER_LIST = this.configuration.SERVER_API_URL + '/users';
    this.USER_ADD = this.configuration.SERVER_API_URL + '/users/';

    this.PROJECT_LIST = this.configuration.SERVER_API_URL + '/project';
    this.PROJECT_DETAIL = this.configuration.SERVER_API_URL + '/project/{{projectId}}/view';
    this.PROJECT_OVERVIEW = this.configuration.SERVER_API_URL + '/project/{{projectId}}/overview';
    this.PROJECT_DASHBOARD_STATS = this.configuration.SERVER_API_URL + '/project/{{projectId}}/dashboard/stats';
    this.PROJECT_PLATFORM_DETAIL = this.configuration.SERVER_API_URL + '/project/{{projectId}}/platform/{{platformId}}/view';
    this.PROJECT_DEVICE_LIST = this.configuration.SERVER_API_URL + '/project/{{projectId}}/device/list';
    this.PROJECT_NOTIFICATION_SUBMIT = this.configuration.SERVER_API_URL + '/project/{{projectId}}/notification/submit';
    this.PROJECT_PLATFORM_DETAIL = this.configuration.SERVER_API_URL + '/project/{{projectId}}/platform/{{platformId}}/detail';
    this.ANDROID_PERMISSIONS_LIST = this.configuration.SERVER_API_URL + '/project/platform/android/permissions';
    this.TIMEZONE_LIST = this.configuration.SERVER_API_URL + '/timezone/list';
    this.ADD_NEW_PROJECT = this.configuration.SERVER_API_URL + '/project';
    this.ADD_PRODUCT_TO_PROJECT = this.configuration.SERVER_API_URL + '/project/{{projectId}}/product/{{productId}}';
    this.UPDATE_PLATFORM = this.configuration.SERVER_API_URL + '/project/{{projectId}}/platform/{{platformId}}/update';
    this.DEVICE_DETAIL = this.configuration.SERVER_API_URL + '/project/{{projectId}}/device/{{deviceId}}/detail';
    this.DEVICE_TESTING_SETTING = this.configuration.SERVER_API_URL + '/project/{{projectId}}/device/{{deviceId}}/testing/mark';
    this.PLATFORM_LIST = this.configuration.SERVER_API_URL + '/project/platform/types';
    this.ADD_NEW_PLATFORM = this.configuration.SERVER_API_URL + '/project/platform/add';
    this.UPLOAD_IMAGE_URL = this.configuration.SERVER_API_URL + '/asset/project/icon/upload';

    this.LIST_PRODUCTS_AVAILABLE = this.configuration.SERVER_API_URL + '/project/{{projectId}}/products/list/available';
    this.LIST_PRODUCTS_ENABLED = this.configuration.SERVER_API_URL + '/project/{{projectId}}/products/list/enabled';
    this.LIST_PROJECT_SEGMENTS = this.configuration.SERVER_API_URL + '/project/{{projectId}}/segments';

  }
}
