/**
 * Created by mohitgupta on 06/08/18.
 */
import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {EMPTY} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private _authService: AuthService, private _router: Router, private toastr: ToastrService,) {
  }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError((err: HttpErrorResponse, caught: Observable<HttpEvent<any>>) => {
          if (err.status === 401 || err.status === 403) {
            this.toastr.warning('Unauthorized! Please Login Again');
            this._authService.removeToken();
            this._router.navigateByUrl('/login');
            return EMPTY;
          } else {
            return throwError(err);
          }
        })
      );
  }
}
