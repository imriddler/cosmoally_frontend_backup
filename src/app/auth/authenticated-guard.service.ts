/**
 * Created by mohitgupta on 06/08/18.
 */
import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Router, NavigationEnd} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {
  }

  canActivate() {
    if (this.auth.isAuthenticated()) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
