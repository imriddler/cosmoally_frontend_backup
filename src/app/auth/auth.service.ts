/**
 * Created by mohitgupta on 06/08/18.
 */
import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class AuthService {


  constructor(private _cookieService: CookieService) {

  }

  public getToken(): string {
    return this._cookieService.get('backendAccessToken');
  }

  public setToken(accessToken: String): void {
    this._cookieService.set('backendAccessToken', accessToken.toString());
  }


  public getUserId(): string {
    return this._cookieService.get('authUserId');
  }

  public setUserId(userId: String): void {
    this._cookieService.set('authUserId', userId.toString());
  }

  public getUserName(): string {
    return this._cookieService.get('authUserName');
  }

  public setUserName(userName: String): void {
    this._cookieService.set('authUserName', userName.toString());
  }

  public removeToken(): void {
    this._cookieService.delete('backendAccessToken', '/');
    this._cookieService.delete('authUserName', '/');
    this._cookieService.delete('authUserId', '/');
  }

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    return token ? true : false;
  }

  public getUserInfo(): any {

  }

}
