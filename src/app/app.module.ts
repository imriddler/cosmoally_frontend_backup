import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ApiEndPoints, Configuration} from './app.constants';
import {AuthGuard} from './auth/auth-guard.service';
import {AuthService} from './auth/auth.service';
import {AuthenticatedGuard} from './auth/authenticated-guard.service';
import {CookieService} from 'ngx-cookie-service';
import {ToastrModule} from 'ngx-toastr';
import { ModalModule , BsDatepickerModule, TabsModule, BsDropdownModule   } from 'ngx-bootstrap';
import {DashboardModule} from './dashboard/dashboard.module';
import {AuthenticationModule} from './authentication/authentication.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {MarkdownModule} from 'ngx-markdown';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'my-app'}),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    MarkdownModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    DashboardModule,
    AuthenticationModule,
    BsDropdownModule.forRoot(),
  ],
  providers: [
    Configuration,
    ApiEndPoints,
    AuthGuard,
    AuthService,
    AuthenticatedGuard,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
