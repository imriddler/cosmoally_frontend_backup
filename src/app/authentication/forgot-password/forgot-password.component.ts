import {Component, OnInit} from '@angular/core';
import {RestService} from '../../rest.service';
import {BaseResponse} from '../../base-response';
import {ToastrService} from 'ngx-toastr';
import {UserDetailResponse} from '../../dashboard/models/app.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  model = {
    email: ''
  };

  public thisYear: number;

  constructor(
    private restService: RestService,
    private toastr: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.thisYear = (new Date()).getFullYear();
  }

  verifyEmail() {
    // tslint:disable-next-line:max-line-length
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.model.email.trim().length === 0) {
      this.toastr.warning('Please enter email to receive otp.');
      return false;
    } else if (!emailRegex.test(String(this.model.email).toLowerCase())) {
      this.toastr.warning('Please enter valid email address.');
      return false;
    } else {
      return true;
    }
  }

  resetPassword() {
    console.log('This');
    if (this.verifyEmail()) {
      console.log('That');
      this.restService.sendOTP(this.model).subscribe(res => {
        const response = res as UserDetailResponse;
        if (response.status) {
          this.router.navigateByUrl('password/reset/' + response.data.user.id);
        } else {
          if (response.httpStatusCode == 443) {
            this.toastr.warning('Looks like this email is not associated with any account.');
          } else if (response.httpStatusCode == 500) {
            this.toastr.error('Sorry could not send OTP.');
          }
        }
      });
    }
  }

  backToLogin() {
    this.router.navigateByUrl('login');
  }
}
