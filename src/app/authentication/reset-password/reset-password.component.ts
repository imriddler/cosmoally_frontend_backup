import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {RestService} from '../../rest.service';
import {BaseResponse} from '../../base-response';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  model = {
    id: null as number,
    otp : '',
    password : '',
    confirmPassword: ''
  };
  public thisYear: number;


  constructor(
    private router: Router,
    private toastr: ToastrService,
    private restService: RestService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.thisYear = (new Date()).getFullYear();
    this.route.paramMap.subscribe(value => {
      this.model.id = value.get('id') as any as number;
    });
  }

  validateRequest() {
    if (this.model.otp.trim().length === 0) {
      this.toastr.warning('Please enter otp.');
      return false;
    } else if (this.model.password.length === 0) {
      this.toastr.warning('Please enter password.');
      return false;
    } else if (this.model.confirmPassword.length === 0) {
      this.toastr.warning('Please enter confirm password.');
      return false;
    } else if (this.model.password !== this.model.confirmPassword) {
      this.toastr.warning('Password did not match.');
      return false;
    } else if (this.model.password.length < 6) {
      this.toastr.warning('Password length should be greater than 6 characters.');
      return false;
    } else if (this.model.password.length > 20) {
      this.toastr.warning('Password length should be less than 20 characters.');
      return false;
    } else {
      return true;
    }
  }

  resetPassword() {
    if (this.validateRequest()) {
      this.restService.resetPassword(this.model).subscribe(res => {
        const response = res as BaseResponse;
        this.toastr.success('Password reset successfully.');
        this.backToLogin();
      }, errRes => {
        this.toastr.error(errRes.error.message, 'Oops');
      });
    }
  }

  backToLogin() {
    this.router.navigateByUrl('login');
  }

}
