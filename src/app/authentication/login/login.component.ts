import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';
import { AuthService } from '../../auth/auth.service';


// @ts-ignore
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.scss'
  ],
  providers: [
    LoginService
  ]
})
export class LoginComponent implements OnInit {

  public model: any;
  public thisYear: number;

  constructor(private _loginService: LoginService,
    private _router: Router,
    private _toastr: ToastrService,
    private _authService: AuthService
  ) {
    this.model = {
      email: '',
      password: ''
    };
    this.thisYear = (new Date()).getFullYear();
  }

  ngOnInit() {
    if (this._authService.isAuthenticated()) {
      this._router.navigate(['/']);
    }
  }

  submitLoginForm() {
    this._loginService.login(this.model).subscribe(response => {
      this._authService.setToken(response.data.token);
      this._authService.setUserId(response.data.user.id);
      this._authService.setUserName(response.data.user.name);
      this._router.navigate(['/']);
    },
    error => {
      if (error && error.error && error.error.message) {
        this._toastr.error(error.error.message, 'Opps!');
      } else {
        this._toastr.error('Unable to complete your request, please try after sometime.', 'Opps!');
      }
    });
  }

}
