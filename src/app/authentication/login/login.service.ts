import 'rxjs/add/operator/map';

import {HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Configuration} from '../../app.constants';
import {ApiEndPoints} from '../../app.constants';

@Injectable()
export class LoginService {

  constructor(private _http: HttpClient, private _apiEndPoints: ApiEndPoints, private _configuration: Configuration) {

  }

  public login<T>(model: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept' : 'application/json'
      })
    };
    return this._http.post<any>(this._apiEndPoints.LOGIN_REQUEST, {
      email : model.email,
      password: model.password
    }, httpOptions);
  }
}
