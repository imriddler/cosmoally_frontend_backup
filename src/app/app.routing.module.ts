import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedGuard } from './auth/authenticated-guard.service';
import { AuthGuard } from './auth/auth-guard.service';

const routes: Routes = [
  {
    path: 'login',
    redirectTo: '/login',
    pathMatch: 'full',
    canActivate: [
      AuthenticatedGuard
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/users',
    canActivate: [AuthGuard],
    // loadChildren: () => DashboardModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
