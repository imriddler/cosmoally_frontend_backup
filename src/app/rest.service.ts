import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserAddModel} from './dashboard/models/app.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  options: any;

  constructor(private http: HttpClient) {
  }


  // apiUrl = 'http://api.cosmoally.demoexample.xyz/api/v1/backend/';
  // apiUrl = 'http://localhost:8000/api/v1/backend/';
  apiUrl = 'http://api.cosmoally.io/api/v1/backend/';


  // Image Upload Method

  uploadImages(fileToUpload: File[]) {
    const formData: FormData = new FormData();
    for (let i = 0; i < fileToUpload.length; i++) {
      formData.append('images[]', fileToUpload[i]);
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'image/add', formData, httpOptions);
  }

  deleteImage(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'image/delete/' + id, httpOptions);
  }


  // Page Services

  deletePageImage(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'page/image/delete/' + id, httpOptions);
  }

  getAppUpdateList(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.get(`${this.apiUrl}app-updates/list`, httpOptions);
  }

  addUpdate(model){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.post(`${this.apiUrl}app-updates/add`, model,httpOptions);
  }

  deleteUpdate(id){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    }
    return this.http.get(`${this.apiUrl}app-updates/delete/${id}`, httpOptions);
  }

  getPageDetail(id: Number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'page/view/' + id, httpOptions);
  }

  deletePage(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'page/delete/' + id, httpOptions);
  }

  getPageList(pageType: Number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'page/list/' + pageType, httpOptions);
  }

  submitPage(model, id = 0) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    if (id === 0) {
      return this.http.post(this.apiUrl + 'page/add', model, httpOptions);
    } else {
      return this.http.post(this.apiUrl + 'page/add/' + id, model, httpOptions);
    }
  }


  // KnowLedge Box

  getFaqList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/list', httpOptions);
  }

  getFaq(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/view/' + id, httpOptions);
  }

  addFaq(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'faq/add', model, httpOptions);
  }

  updateFaq(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'faq/add/' + model.id, model, httpOptions);
  }

  deleteFaq(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/delete/' + id, httpOptions);
  }

  getFaqTypeList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/type/list', httpOptions);
  }

  addCategory(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'faq/type/add', model, httpOptions);
  }

  editFaqCategory(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'faq/type/add/' + model.id, model, httpOptions);
  }

  getFaqCategoryDetail(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/type/view/' + id, httpOptions);
  }

  deleteCategory(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'faq/type/delete/' + id, httpOptions);
  }

  getBannerList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'banner/list');
  }

  addBanner(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'banner/add', model, httpOptions);
  }

  deleteBanner(bannerId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'banner/delete/' + bannerId, httpOptions);
  }


  // User Management
  getUserList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/list-admin', httpOptions);
  }

  getAppUserList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/list-user', httpOptions);
  }


  deleteAppUser(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/delete-user/' + id, httpOptions);
  }


  addUser(model: UserAddModel) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/add', model, httpOptions);
  }

  editUser(model: UserAddModel) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/edit', model, httpOptions);
  }

  changePassword(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/change-password', model, httpOptions);
  }

  getUserDetails(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/detail/' + id, httpOptions);
  }

  deleteUser(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/delete/' + id, httpOptions);
  }


  // Product Services
  getProductCategoryList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/category/list', httpOptions);
  }

  addProductCategory(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'product/category/add', model, httpOptions);
  }

  editProductCategory(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'product/category/add/' + model.id, model, httpOptions);
  }

  viewProductCategoryDetail(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/category/detail/' + id, httpOptions);
  }

  deleteProductCategory(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/category/delete/' + id, httpOptions);
  }

  getCategorizedProductList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/categorized-list', httpOptions);
  }

  getUnCategorizedProductList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/uncategorized-list', httpOptions);
  }

  categorizeProduct(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'product/assign-category', model, httpOptions);
  }

  // Notification


  getNotificationList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'notification/list', httpOptions);
  }

  sendNotification(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'notification/add', model, httpOptions);
  }

  getNotificationDetails(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'notification/detail/' + id, httpOptions);
  }

  getNotificationUsers(notificationId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'notification/user-list/' + notificationId, httpOptions);
  }

  getProductBatchList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/batch/list', httpOptions);
  }

  getBatchPreview(batch: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/batch/preview/' + batch, httpOptions);
  }

  getProductList(batch) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/batch/product-list/' + batch, httpOptions);
  }

  getProductDetail(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/detail/' + id, httpOptions);
  }

  // Reset Password


  // verifyEmail(model) {
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json'
  //     })
  //   };
  //   return this.http.post(this.apiUrl + 'verify-user', model, httpOptions);
  // }

  sendOTP(model) {
    const httOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/send-otp', model, httOptions);
  }

  resetPassword(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/reset-password', model, httpOptions);
  }

  getProductCategoryDetail(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/category/detail/' + id, httpOptions);
  }

  getProductStatusList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'product/status-list', httpOptions);
  }

  updateProduct(model, batchNumber) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'batch/update/' + batchNumber, model, httpOptions);
  }

  getBatchLog(batchNumber: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'batch/logs/' + batchNumber, httpOptions);
  }


  // Services Rest Methods

  getServiceList(listType) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'service/list/' + listType, httpOptions);
  }

  addService(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'service/add', model, httpOptions);
  }

  getServiceDetail(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'service/detail/' + id, httpOptions);
  }

  updateService(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'service/update/' + model.id, model, httpOptions);
  }

  deleteService(serviceId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'service/delete/' + serviceId, httpOptions);
  }

  updateNotification(model) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'notification/update/' + model.id, model, httpOptions);
  }

  getUsersDropdownList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'notification/users', httpOptions);
  }


  // Sort Order Helper

  handleSortOrderInput(event) {
    if (event.which === 189 || event.which === 69 || event.which === 187 || event.which === 190) {
      event.preventDefault();
    }
  }

  editBanner(model, bannerId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'banner/update/' + bannerId, model, httpOptions);
  }


  getUserFaqReadList(userId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/faq-read/' + userId, httpOptions);
  }

  blockUser(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/block-user/' + id, httpOptions);
  }

  unBlockUser(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'user/unblock-user/' + id, httpOptions);
  }

  getEnquiries() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'enquiries/list', httpOptions);
  }

  getEnquiryDetail(enqueryId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.apiUrl + 'enquiries/detail/' + enqueryId, httpOptions);
  }

  updateMaxFaqRead(maxFaqReadModel) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
    return this.http.post(this.apiUrl + 'user/set-max-faq-read', maxFaqReadModel, httpOptions);
  }
}
